<?php
//MyCC
$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Enrollment::class,
    function (Faker\Generator $faker) {
        return [
            'ENRL_EMPLID'              => $faker->randomNumber(8),
            'ENRL_ACAD_CAREER'         => 'CRED',
            'ENRL_ACTN_RSN_LAST_CD'    => $faker->randomElement(['E', 'W']),
            'ENRL_ACTN_RSN_LAST_LDESC' => $faker->text(32),
            'ENRL_CLASS_NBR'           => $faker->randomNumber(5),
            'ENRL_GRADE_DATE'          => $faker->optional(0.1)->date('Y-m-d', '-1 day'),
            'ENRL_GRADE_POINTS'        => 4,
            'ENRL_GRADING_BASIS'       => 'GRD',
            'ENRL_INSTITUTION_CD'      => 'COLL01',
            'ENRL_OFFICIAL_GRADE'      => 'A',
            'ENRL_STATUS_REASON_CD'    => $faker->randomElement(['ENRL', 'DROP']),
            'ENRL_TERM_CD'             => '1234',
            'ENRL_UNITS_TAKEN'         => 3,
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Instructor::class,
    function (Faker\Generator $faker) use ($factory) {
        return $factory->raw(\Smorken\SisDb\Models\Eloquent\Cds\Person::class);
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Klass::class,
    function (Faker\Generator $faker) {
        return [
            'CASSC_GRADING_BASIS'     => $faker->randomElement(['PZ', 'GRD']),
            'CASSC_HONORS_YN'         => 'N',
            'CASSC_UNITS_MINIMUM'     => 3,
            'CLASS_ACAD_CAREER'       => 'CRED',
            'CLASS_ACAD_ORG'          => 'foo',
            'CLASS_ACAD_ORG_LDESC'    => 'Foo Dept',
            'CLASS_CATALOG_NBR'       => $faker->randomNumber(3),
            'CLASS_CLASS_NAME'        => $faker->text(32),
            'CLASS_CLASS_NBR'         => $faker->randomNumber(5),
            'CLASS_CLASS_STAT'        => 'A',
            'CLASS_COMPONENT_CD'      => $faker->randomElement(['LAB', 'L+L', 'LEC']),
            'CLASS_COURSE_ID'         => $faker->randomNumber(1),
            'CLASS_COURSE_OFFER_NBR'  => $faker->randomNumber(1),
            'CLASS_DAY_EVE'           => 'D',
            'CLASS_DESCR'             => $faker->text(64),
            'CLASS_END_DATE'          => \Carbon\Carbon::instance($faker->dateTimeBetween('+1 week', '+1 month')),
            'CLASS_ENRL_CAP'          => 25,
            'CLASS_ENRL_STAT'         => 'O',
            'CLASS_ENRL_TOT'          => 20,
            'CLASS_INSTITUTION_CD'    => 'COLL01',
            'CLASS_INSTRUCTION_MODE'  => $faker->randomElement(['P', 'IN', 'HY']),
            'CLASS_INSTRUCTOR_EMPLID' => $faker->randomNumber(8),
            'CLASS_LOCATION_CD'       => 'MAIN',
            'CLASS_SECTION'           => $faker->randomNumber(4),
            'CLASS_SESSION_CD'        => 'DD',
            'CLASS_START_DATE'        => \Carbon\Carbon::instance($faker->dateTimeBetween('-1 month', '-1 week')),
            'CLASS_SUBJECT_CD'        => 'ABC',
            'CLASS_TERM_CD'           => '1234',
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Meeting::class,
    function (Faker\Generator $faker) {
        return [
            'CLASS_CLASS_NBR'           => $faker->randomNumber(5),
            'CLASS_INSTITUTION_CD'      => 'COLL01',
            'CLASS_TERM_CD'             => '1234',
            'CLASSM_BLDG_CD'            => 'A BLDG',
            'CLASSM_CLASS_MTG_NBR'      => $faker->randomNumber(1),
            'CLASSM_COURSE_ID'          => $faker->randomNumber(1),
            'CLASSM_COURSE_OFFER_NBR'   => $faker->randomNumber(1),
            'CLASSM_DAYS_PATTERN'       => 'MWF',
            'CLASSM_END_DATE'           => \Carbon\Carbon::instance($faker->dateTimeBetween('+1 week', '+1 month')),
            'CLASSM_FACILITY_ID'        => 'PC101',
            'CLASSM_FACILITY_TYPE'      => 'ROOM',
            'CLASSM_LOCATION'           => 'COLL01',
            'CLASSM_MEETING_TIME_END'   => null,
            'CLASSM_MEETING_TIME_START' => null,
            'CLASSM_ROOM'               => '101',
            'CLASSM_SECTION'            => $faker->randomNumber(4),
            'CLASSM_SESSION_CD'         => 'DD',
            'CLASSM_START_DATE'         => \Carbon\Carbon::instance($faker->dateTimeBetween('-1 month', '-1 week')),
            'CLASSM_TERM_CD'            => '1234',
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\KlassInstructor::class,
    function (Faker\Generator $faker) {
        return [
            'CLASS_CLASS_NBR'          => $faker->randomNumber(5),
            'CLASS_INSTITUTION_CD'     => 'COLL01',
            'CLASS_TERM_CD'            => '1234',
            'CLASSM_ASSIGN_TYPE'       => 'PAY',
            'CLASSM_CLASS_MTG_NBR'     => $faker->randomNumber(1),
            'CLASSM_COURSE_ID'         => $faker->randomNumber(1),
            'CLASSM_COURSE_OFFER_NBR'  => $faker->randomNumber(1),
            'CLASSM_INSTR_ASSIGN_SEQ'  => $faker->randomNumber(1),
            'CLASSM_INSTR_EMAIL'       => $faker->safeEmail,
            'CLASSM_INSTR_LOAD_FACTOR' => '100',
            'CLASSM_INSTR_NAME_FLAST'  => 'A. ' . $faker->lastName,
            'CLASSM_INSTR_TYPE'        => 'ADJ',
            'CLASSM_INSTRUCTOR_EMPLID' => $faker->randomNumber(8),
            'CLASSM_INSTRUCTOR_NAME'   => $faker->name,
            'CLASSM_INSTRUCTOR_ROLE'   => 'PI',
            'CLASSM_SECTION'           => $faker->randomNumber(4),
            'CLASSM_SESSION_CD'        => 'DD',
            'CLASSM_TERM_CD'           => '1234',
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Material::class,
    function (Faker\Generator $faker) {
        return [
            'CLASS_ACAD_CAREER'      => 'CRED',
            'CLASS_CLASS_NBR'        => $faker->randomNumber(5),
            'CLASS_INSTITUTION_CD'   => 'COLL01',
            'CLASS_TERM_CD'          => '1234',
            'TBMAT_AUTHOR'           => $faker->name,
            'TBMAT_DISPLAY_SEQUENCE' => $faker->randomNumber(1),
            'TBMAT_EDITION'          => $faker->text(10),
            'TBMAT_ISBN_NBR'         => $faker->randomNumber(15),
            'TBMAT_NOTES'            => $faker->words(3, true),
            'TBMAT_PRICE'            => $faker->randomFloat(2, 1, 100),
            'TBMAT_PUBLISHER'        => $faker->name,
            'TBMAT_SEQ_NBR'          => $faker->randomNumber(1),
            'TBMAT_STATUS_CD'        => $faker->randomElement(['REQ', 'CH']),
            'TBMAT_STATUS_LDESC'     => $faker->text(16),
            'TBMAT_TITLE'            => $faker->words(3, true),
            'TBMAT_TYPE_CD'          => $faker->randomElement(['TEXTBOOK', 'EBOOK', 'PACKAGE']),
            'TBMAT_TYPE_CD_LDESC'    => $faker->text(16),
            'TBMAT_YEAR_PUBLISHED'   => $faker->date('Y', '-2 years'),
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\Cds\Person::class,
    function (Faker\Generator $faker) {
        return [
            'PERS_EMPLID'             => $faker->randomNumber(8),
            'PERS_OPRID'              => str_random(10),
            'PERS_PRIMARY_FIRST_NAME' => $faker->firstName,
            'PERS_PRIMARY_LAST_NAME'  => $faker->lastName,
            'PERS_EMAIL_ADDR'         => $faker->safeEmail,
            'PERS_BIRTHDATE'          => $faker->date(),
            'HRMS_EMPLID'             => $faker->randomNumber(8),
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\ServiceIndicator::class,
    function (Faker\Generator $faker) {
        return [
            'SRVC_EMPLID'                   => $faker->randomNumber(8),
            'SRVC_IND_DATETIME'             => $faker->dateTimeBetween('-1 year', '-1 day'),
            'SRVC_OPRID'                    => $faker->text(10),
            'SRVC_INSTITUTION'              => 'COLL01',
            'SRVC_SERVICE_IND_CD'           => $faker->randomElement(['ADX', 'DBT', 'ADM', 'PKH', 'FAD']),
            'SRVC_SERVICE_IND_LDESC'        => 'Negative indicators',
            'SRVC_SERVICE_IND_REASON'       => str_random(3),
            'SRVC_SERVICE_IND_REASON_LDESC' => $faker->words(3, true),
            'SRVC_SERVICE_IND_ACT_TERM'     => '1234',
            'SRVC_SERVICE_IND_ACTIVE_DT'    => $faker->dateTimeBetween('-1 year', '-1 day'),
            'SRVC_POSITIVE_SRVC_INDICATOR'  => $faker->randomElement(['Y', 'N']),
            'SRVC_EXT_DUE_DT'               => null,
            'SRVC_AMOUNT'                   => $faker->randomFloat(2, 0, 100),
            'SRVC_COMM_COMMENTS'            => $faker->words(5, true),
            'SRVC_CONTACT'                  => $faker->name,
            'SRVC_DEPARTMENT_LDESC'         => $faker->words(2, true),
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Student::class,
    function (Faker\Generator $faker) use ($factory) {
        return $factory->raw(\Smorken\SisDb\Models\Eloquent\Cds\Person::class);
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\StudentTerm::class,
    function (Faker\Generator $faker) {
        return [
            'CTERM_EMPLID'           => $faker->randomNumber(8),
            'CTERM_TERM_CD'          => $faker->randomElement(['1014', '1016', '1022', '1024', '1026', '1234']),
            'CTERM_INSTITUTION_CODE' => 'COLL01',
            'CTERM_ACAD_CAREER'      => 'CRED',
            'STFACT_MCCD_CUM_AHRS'   => rand(10, 60),
            'STFACT_MCCD_CUM_EHRS'   => rand(10, 60),
            'STFACT_MCCD_CUM_QHRS'   => rand(10, 60),
            'STFACT_MCCD_CUM_QPTS'   => $faker->randomFloat(2, 1, 4),
            'STFACT_MCCD_TERM_AHRS'  => rand(1, 15),
            'STFACT_MCCD_TERM_EHRS'  => rand(1, 15),
            'STFACT_MCCD_TERM_QHRS'  => rand(1, 15),
            'STFACT_MCCD_TERM_QPTS'  => $faker->randomFloat(2, 1, 4),
            'CTERM_ACAD_STNDNG_ACTN' => $faker->randomElement(['ACPT', null]),
            'CTERM_ACAD_LOAD_CODE'   => $faker->randomElement(['N', 'L', 'H', 'F', 'T']),
        ];
    }
);

$factory->define(
    \Smorken\SisDb\Models\Eloquent\MyCC\Term::class,
    function (Faker\Generator $faker) {
        return [
            'STRM'          => '1234',
            'DESCR'         => 'Term 1',
            'ACAD_CAREER'   => 'CRED',
            'TERM_BEGIN_DT' => \Carbon\Carbon::instance($faker->dateTimeBetween('-1 month', '-1 week')),
            'TERM_END_DT'   => \Carbon\Carbon::instance($faker->dateTimeBetween('+1 week', '+1 month')),
            'INSTITUTION'   => 'COLL01',
        ];
    }
);
