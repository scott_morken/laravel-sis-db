<?php
//RDS
$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Athlete::class, function(Faker\Generator $faker) {
    return [
        'ATHL_INSTITUTION' => 'COLL01',
        'ATHL_EMPLID' => $faker->randomNumber(8),
        'ATHL_SPORT' => $faker->randomElement(['FB', 'BSK', 'MSC']),
        'ATHL_SPORT_LDESC' => $faker->text(32),
        'ATHL_ATHLETIC_PARTIC_CD' => 'ACTIVE',
        'ATHL_ATHLETIC_PARTIC_LDESC' => 'Active',
        'ATHL_NCAA_ELIGIBLE' => 'Y',
        'ATHL_CURRENT_PARTICIPANT' => 'Y',
        'ATHL_START_DATE' => $faker->date('Y-m-d', '-1 week'),
        'ATHL_END_DATE' => null,
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Degree::class, function(Faker\Generator $faker) {
    return [
        'INSTITUTION' => 'COLL01',
        'ACAD_PROG' => 'CRED',
        'ACAD_PLAN_TYPE' => 'DEG',
        'ACAD_PLAN' => $faker->randomNumber(4),
        'EFFDT' => $faker->dateTimeBetween('-5 years', '-1 year'),
        'EFF_STATUS' => 'A',
        'FIRST_TERM_VALID' => '1234',
        'DESCR' => $faker->words(3, true),
        'DEGREE' => 'AAS',
        'DEGREE_DESCR' => 'Associate in Applied Science',
        'DIPLOMA_DESCR' => $faker->words(3, true),
        'TRNSCR_DESCR' => $faker->words(3, true),
        'MIN_UNITS_REQD' => 60,
        'PLAN_DESCRIPTION' => $faker->paragraphs(2, true),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Group::class, function(Faker\Generator $faker) {
    return [
        'STDGR_INSTITUTION' => 'COLL01',
        'STDGR_EMPLID' => $faker->randomNumber(8),
        'STDGR_STUDENT_GROUP' => $faker->toUpper($faker->text(5)),
        'STDGR_STDNT_GROUP_LDESC' => $faker->words(3, true),
        'STDGR_START_DATE' => $faker->dateTimeBetween('-2 years', '-6 months'),
        'STDGR_END_DATE' => $faker->date('Y-m-d', '+1 week'),
        'STDGR_STUDENT_STATUS' => 'A',
        'STDGR_STUDENT_STATUS_LDESC' => 'Active',
        'STDGR_COMMENTS' => $faker->words(5, true),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Enrollment::class, function(Faker\Generator $faker) {
    return [
        'CLASS_SID' => $faker->randomNumber(9),
        'EMPLID' => $faker->randomNumber(8),
        'ENRL_INSTITUTION_CD' => 'COLL01',
        'ENRL_TERM_CD' => '1234',
        'ENRL_CLASS_NBR' => $faker->randomNumber(5),
        'ENRL_ACAD_CAREER' => 'CRED',
        'ENRL_GRADING_BASIS' => 'GRD',
        'ENRL_OFFICIAL_GRADE' => 'A',
        'ENRL_GRADE_DATE' => $faker->optional(0.1)->date('Y-m-d', '-1 day'),
        'ENRL_GRADE_POINTS' => 4,
        'ENRL_UNITS_TAKEN' => 3,
        'ENRL_STATUS_REASON_CD' => $faker->randomElement(['ENRL', 'DROP']),
        'ENRL_STATUS_REASON_LDESC' => $faker->text(32),
        'ENRL_ACTN_RSN_LAST_CD' => $faker->randomElement(['E', 'W']),
        'ENRL_ACTN_RSN_LAST_LDESC' => $faker->text(32),
        'ENRL_INCLUDE_IN_GPA' => 'Y',
    ];
});
$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Klass::class, function(Faker\Generator $faker) {
    return [
        'CLASS_SID' => $faker->randomNumber(9),
        'COFFR_CLASS_SID' => $faker->randomNumber(3),
        'CLASS_COURSE_ID' => $faker->randomNumber(1),
        'CLASS_COURSE_OFFER_NBR' => $faker->randomNumber(1),
        'CLASS_TERM_CD' => '1234',
        'CLASS_SESSION_CD' => 'DD',
        'CLASS_SECTION' => $faker->randomNumber(4),
        'CLASS_ACAD_CAREER' => 'CRED',
        'CLASS_INSTITUTION_CD' => 'COLL01',
        'CLASS_CLASS_NBR' => $faker->randomNumber(5),
        'CLASS_ACAD_ORG' => 'foo',
        'CLASS_ACAD_ORG_LDESC' => 'Foo Dept',
        'CLASS_CLASS_NAME' => $faker->text(32),
        'CLASS_SUBJECT_CD' => 'ABC',
        'CLASS_CATALOG_NBR' => $faker->randomNumber(3),
        'CLASS_DESCR' => $faker->text(64),
        'CASSC_UNITS_MINIMUM' => 3,
        'CASSC_GRADING_BASIS' => $faker->randomElement(['PZ', 'GRD']),
        'CLASS_ENRL_STAT' => 'O',
        'CLASS_CLASS_STAT' => 'A',
        'CLASS_INSTRUCTION_MODE' => $faker->randomElement(['P','IN', 'HY']),
        'CLASS_INSTRUCTION_MODE_LDESC' => $faker->text(32),
        'CLASS_INSTRUCTOR_EMPLID' => $faker->randomNumber(8),
        'CLASS_INSTRUCTOR_NAME' => $faker->name,
        'CLASS_DAY_EVE' => 'D',
        'CLASS_DAY_EVE_LDESC' => $faker->randomElement(['DAY', 'EVE']),
        'CLASS_START_DATE' => \Carbon\Carbon::instance($faker->dateTimeBetween('-1 month', '-1 week')),
        'CLASS_END_DATE' => \Carbon\Carbon::instance($faker->dateTimeBetween('+1 week', '+1 month')),
        'CLASS_ENRL_TOT' => 20,
        'CLASS_ENRL_CAP' => 25,
        'CASSC_HONORS_YN' => 'N',
        'CLASS_COMPONENT_CD' => $faker->randomElement(['LAB', 'L+L', 'LEC']),
        'CLASS_COMPONENT_LDESC' => $faker->text(32),
        'CLASS_DEPTID_EXPENSE_CD' => $faker->randomNumber(5),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Material::class, function(Faker\Generator $faker) {
    return [
        'TBMAT_SID' => $faker->randomNumber(9),
        'TBMAT_SEQ_NBR' => $faker->randomNumber(1),
        'TBMAT_TYPE_CD' => $faker->randomElement(['TEXTBOOK', 'EBOOK', 'PACKAGE']),
        'TBMAT_TYPE_CD_LDESC' => $faker->text(16),
        'TBMAT_STATUS_CD' => $faker->randomElement(['REQ', 'CH']),
        'TBMAT_STATUS_LDESC' => $faker->text(16),
        'TBMAT_TITLE' => $faker->words(3, true),
        'TBMAT_ISBN_NBR' => $faker->randomNumber(15),
        'TBMAT_AUTHOR' => $faker->name,
        'TBMAT_PUBLISHER' => $faker->name,
        'TBMAT_EDITION' => $faker->text(10),
        'TBMAT_YEAR_PUBLISHED' => $faker->date('Y', '-2 years'),
        'TBMAT_PRICE' => $faker->randomFloat(2, 1, 100),
        'TBMAT_NOTES' => $faker->words(3, true),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Person::class, function (Faker\Generator $faker) {
    return [
        'EMPLID' => $faker->randomNumber(8),
        'PERS_OPRID' => str_random(10),
        'PERS_PREFERRED_FIRST_NAME' => $faker->firstName,
        'PERS_PERFERRED_LAST_NAME' => $faker->lastName,
        'PERS_PRIMARY_FIRST_NAME' => $faker->firstName,
        'PERS_PRIMARY_LAST_NAME' => $faker->lastName,
        'EMAIL_EMAIL_ADDR' => $faker->safeEmail,
        'PERS_BIRTHDATE' => $faker->date(),
        'HRMS_EMPLID' => $faker->randomNumber(8),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Program::class, function (Faker\Generator $faker) {
    return [
        'STRUC_EMPLID' => $faker->randomNumber(8),
        'STRUC_INSTITUTION' => 'COLL01',
        'STRUC_ACAD_PROG_LDESC' => 'Degree and Cert Programs',
        'STRUC_ACAD_PROG_STATUS' => 'AC',
        'STRUC_ACAD_PROG_STATUS_LDESC' => 'Active',
        'STRUC_PROG_REASON_LDESC' => 'Active in Program',
        'STRUC_PROG_ADMIT_TERM' => '1234',
        'STRUC_ACAD_PLAN_LDESC' => 'Associate in FooBar',
        'STRUC_PLAN_TYPE_LDESC' => 'Degree',
        'STRUC_ACAD_PLAN_DEGREE' => 'AAS',
        'STRUC_EXP_GRAD_TERM' => '2345',
        'STRUC_ACAD_INTEREST_LDESC' => null,
        'STRUC_DEGR_CHECKOUT_STAT' => 'AW',
        'STRUC_DEGR_CHECKOUT_LDESC' => 'Awarded',
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\ServiceIndicator::class, function (Faker\Generator $faker) {
    return [
        'SRVC_EMPLID' => $faker->randomNumber(8),
        'SRVC_IND_DATETIME' => $faker->dateTimeBetween('-1 year', '-1 day'),
        'SRVC_OPRID' => $faker->text(10),
        'SRVC_INSTITUTION' => 'COLL01',
        'SRVC_SERVICE_IND_CD' => $faker->randomElement(['ADX', 'DBT', 'ADM', 'PKH', 'FAD']),
        'SRVC_SERVICE_IND_LDESC' => 'Negative indicators',
        'SRVC_SERVICE_IND_REASON' => str_random(3),
        'SRVC_SERVICE_IND_REASON_LDESC' => $faker->words(3, true),
        'SRVC_SERVICE_IND_ACT_TERM' => '1234',
        'SRVC_SERVICE_IND_ACTIVE_DT' => $faker->dateTimeBetween('-1 year', '-1 day'),
        'SRVC_POSITIVE_SRVC_INDICATOR' => $faker->randomElement(['Y', 'N']),
        'SRVC_AMOUNT' => $faker->randomFloat(2, 0, 100),
        'SRVC_COMM_COMMENTS' => $faker->words(5, true),
        'SRVC_CONTACT' => $faker->name,
        'SRVC_DEPARTMENT_LDESC' => $faker->words(2, true),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Student::class, function (Faker\Generator $faker) use ($factory) {
    return $factory->raw(\Smorken\SisDb\Models\Eloquent\Rds\Person::class);
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\StudentTerm::class, function (Faker\Generator $faker) {
    return [
        'EMPLID' => $faker->randomNumber(8),
        'CTERM_TERM_CD' => $faker->randomElement(['1014', '1016', '1022', '1024', '1026', '1234']),
        'CTERM_TERM_SDESC' => 'Term Random',
        'CTERM_INSTITUTION_CODE' => 'COLL01',
        'CTERM_ACAD_CAREER' => 'CRED',
        'STFACT_CUM_GPA' => $faker->randomFloat(2, 1, 4),
        'STFACT_TERM_GPA' => $faker->randomFloat(2, 1, 4),
        'STFACT_MCCD_CUM_AHRS' => rand(10, 60),
        'STFACT_MCCD_CUM_EHRS' => rand(10, 60),
        'STFACT_MCCD_CUM_QHRS' => rand(10, 60),
        'STFACT_MCCD_CUM_QPTS' => $faker->randomFloat(2, 1, 4),
        'STFACT_MCCD_TERM_AHRS' => rand(1, 15),
        'STFACT_MCCD_TERM_EHRS' => rand(1, 15),
        'STFACT_MCCD_TERM_QHRS' => rand(1, 15),
        'STFACT_MCCD_TERM_QPTS' => $faker->randomFloat(2, 1, 4),
        'CTERM_ACAD_STDNG_LDESC' => 'A OK',
        'CTERM_ACAD_LOAD_SDESC' => $faker->randomElement(['N', 'L', 'H', 'F', 'T']),
        'STFACT_TOT_INPROG_GPA' => $faker->randomFloat(1, 0, 4),
        'STFACT_TOT_INPROG_NOGPA' => $faker->randomFloat(1, 0, 4),
        'CTERM_ACAD_STNDNG_ACTN' => $faker->randomElement(['ACPT', null]),
        'CTERM_ACAD_LOAD_CODE' => $faker->randomElement(['N', 'L', 'H', 'F', 'T']),
    ];
});

$factory->define(\Smorken\SisDb\Models\Eloquent\Rds\Term::class, function (Faker\Generator $faker) {
    return [
        'STRM' => '1234',
        'DESCR' => 'Term 1',
        'TERM_BEGIN_DT' => \Carbon\Carbon::instance($faker->dateTimeBetween('-1 month', '-1 week')),
        'TERM_END_DT' => \Carbon\Carbon::instance($faker->dateTimeBetween('+1 week', '+1 month')),
        'INSTITUTION' => 'COLL01',
    ];
});
