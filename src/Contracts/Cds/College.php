<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/10/15
 * Time: 10:38 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

/**
 * Interface College
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property string $id
 * @property string $description
 * @property string $abbrev
 */
interface College
{

}
