<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/2/15
 * Time: 11:53 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

use Carbon\Carbon;

/**
 * Interface Enrollment
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $acad_career
 * @property $class_number
 * @property $college_id
 * @property $grade
 * @property $grade_points
 * @property $grading_basis
 * @property $last_action_code
 * @property $last_action_desc
 * @property $status_code
 * @property $status_desc
 * @property $student_id
 * @property $term_id
 * @property $units_taken
 * @property Carbon $grade_date
 *
 * @property Klass[] $klass
 * @property Student[] $student
 */
interface Enrollment
{

}
