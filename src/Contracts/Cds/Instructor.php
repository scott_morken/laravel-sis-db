<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:01 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

/**
 * Interface Instructor
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $id
 * @property $alt_id
 * @property $hrms_id
 * @property $first_name
 * @property $last_name
 * @property $email
 * @property $dob
 */
interface Instructor extends Person
{

}
