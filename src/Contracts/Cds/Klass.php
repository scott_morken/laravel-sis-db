<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:02 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

use Carbon\Carbon;
use Smorken\SisDb\Contracts\Rds\Material;

/**
 * Interface Klass
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $acad_career
 * @property $acad_org
 * @property $acad_org_desc
 * @property $catalog_number
 * @property $class_name
 * @property $class_number
 * @property $college_id
 * @property $component_code
 * @property $component_desc
 * @property $course_id
 * @property $course_offer_number
 * @property $day_evening_code
 * @property $description
 * @property $enrolled
 * @property $enrolled_cap
 * @property $enrollment_status
 * @property $enrollment_status_desc
 * @property $grading_basis
 * @property $honors
 * @property $instruction_mode
 * @property $instruction_mode_desc
 * @property $location_code
 * @property $section
 * @property $session_code
 * @property $status
 * @property $status_desc
 * @property $subject
 * @property $term_id
 * @property $units
 * @property Carbon $end_date
 * @property Carbon $start_date
 *
 * @property Material[] $materials
 * @property Enrollment[] $enrollments
 * @property Meeting[] $meetings
 * @property Instructor[] $instructors
 */
interface Klass
{

}
