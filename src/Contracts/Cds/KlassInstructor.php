<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:07 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

/**
 * Interface KlassInstructor
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $sequence_number
 * @property $college_id
 * @property $instructor_id
 * @property $full_name
 * @property $short_name
 * @property $type_code
 * @property $type_desc
 * @property $role_code
 * @property $role_desc
 * @property $load
 * @property $assignment_type_code
 *
 * @property Klass $klass
 * @property Meeting[] $meetings
 * @property Instructor $instructor
 */
interface KlassInstructor
{

}
