<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/1/17
 * Time: 8:20 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

/**
 * Interface KlassNote
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property int $sequence_number
 * @property string $note_number
 * @property string $type
 * @property string $description
 * @property string $text
 * @property string $college_id
 */
interface KlassNote
{

}
