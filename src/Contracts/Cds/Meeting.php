<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:04 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

use Carbon\Carbon;

/**
 * Interface Meeting
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $meeting_id  Sequence number
 * @property $college_id
 * @property $building_code  A PC HE/A ONLINE/A G BLDG
 * @property $location_code  PC MAIN/PC HEALTH
 * @property $room  Room name/TBA/ONLINE CNV/DNTL CLIN (same as facility id)
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property $days
 *
 * @property Klass $klass
 * @property Instructor[] $instructors
 */
interface Meeting
{

    public function daysAsString($long = false);

    public function primaryInstructor();
}
