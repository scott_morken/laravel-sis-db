<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/2/15
 * Time: 11:53 AM
 */

namespace Smorken\SisDb\Contracts\Cds;

/**
 * Interface StudentTerm
 * @package Smorken\SisDb\Contracts\Cds
 *
 * @property $student_id
 * @property $term_id
 * @property $acad_career
 * @property $cumu_gpa
 * @property $term_gpa
 * @property $cumu_attempted_hours
 * @property $cumu_earned_hours
 * @property $cumu_gpa_hours
 * @property $cumu_gpa_points
 * @property $term_attempted_hours
 * @property $term_earned_hours
 * @property $term_gpa_hours
 * @property $term_gpa_points
 * @property $academic_standing_code
 * @property $academic_standing_desc
 * @property $load_code
 * @property $load_desc
 *
 * @property Student $student
 */
interface StudentTerm
{

}
