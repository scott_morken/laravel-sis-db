<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/1/16
 * Time: 12:56 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;

/**
 * Interface Athlete
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $college_id
 * @property $student_id
 * @property $sport_code
 * @property $sport_desc
 * @property $status_code
 * @property $status_desc
 * @property $ncaa_eligible
 * @property $current_participant
 * @property Carbon $start_date
 * @property Carbon $end_date
 *
 * @property Student $student
 * @property Enrollment[] $enrollments
 */
interface Athlete
{

}
