<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 9:58 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface College
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $id
 * @property $description
 */
interface College
{

}
