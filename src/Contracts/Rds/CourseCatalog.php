<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/21/17
 * Time: 10:30 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Illuminate\Support\Collection;

/**
 * Interface CourseCatalog
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property string $id
 * @property int $catalog_id
 * @property string $course_id
 * @property string $description
 * @property string $consent
 * @property string $component_code
 * @property float $units
 * @property float $units_max
 * @property string $grading_basis
 * @property string $notes
 * @property string $approval_date
 * @property string $effective_date
 *
 * @property Collection<CourseOffer> $offerings
 */
interface CourseCatalog
{

}
