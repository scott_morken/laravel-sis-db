<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/21/17
 * Time: 10:45 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface CourseOffer
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property int $catalog_id
 * @property int $class_id
 * @property int $offer_number
 * @property string $acad_career
 * @property string $acad_group
 * @property string $acad_org
 * @property string $approved
 * @property string $campus
 * @property string $catalog_number
 * @property string $catalog_print
 * @property string $college_id
 * @property string $effective_date
 * @property string $id
 * @property string $schedule_print
 * @property string $subject
 *
 * @property CourseCatalog $courseCatalog
 */
interface CourseOffer
{

}
