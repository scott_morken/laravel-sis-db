<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/2/16
 * Time: 2:25 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;

/**
 * Interface Degree
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $plan_code
 * @property $college_id
 * @property $acad_prog_code
 * @property $plan_type_code
 * @property Carbon $effective_date
 * @property $status
 * @property $term_active
 * @property $short_desc
 * @property $degree_code
 * @property $degree_desc
 * @property $diploma_desc
 * @property $transcript_desc
 * @property $credits_required
 * @property $long_desc
 */
interface Degree
{

}
