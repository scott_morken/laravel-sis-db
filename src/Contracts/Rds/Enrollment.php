<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 12:25 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;

/**
 * Interface Enrollment
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $student_id
 * @property $college_id
 * @property $term_id
 * @property $class_number
 * @property $acad_career
 * @property $grading_basis
 * @property $grade
 * @property Carbon $grade_date
 * @property $units_taken
 * @property $grade_points
 * @property $status_desc
 * @property $last_action_code
 * @property $last_action_desc
 *
 * @property Klass $klass
 * @property Student $student
 * @property Material[] $materials
 */
interface Enrollment
{

}
