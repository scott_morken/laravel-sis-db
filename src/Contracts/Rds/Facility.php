<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/9/17
 * Time: 12:14 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Facility
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property string $id
 * @property string $college_id
 * @property string $status
 * @property string $building_code
 * @property string $room
 * @property string $description
 * @property string $short_descr
 * @property string $group
 * @property string $location_code
 * @property int $capacity
 * @property string $acad_org
 */
interface Facility
{

}
