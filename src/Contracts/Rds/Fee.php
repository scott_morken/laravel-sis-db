<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/2/17
 * Time: 12:03 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Fee
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property string $college_id
 * @property string $term_id
 * @property string $account_type
 * @property string $item_type
 * @property string $short_description
 * @property string $description
 * @property float $amount
 * @property string $currency
 */
interface Fee
{

}
