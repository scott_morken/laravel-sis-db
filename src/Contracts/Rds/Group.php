<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/19/16
 * Time: 9:29 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;

/**
 * Interface Group
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $id
 * @property $student_id
 * @property $college_id
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property $description
 * @property $status_code
 * @property $status_desc
 * @property $comments
 */
interface Group
{

}
