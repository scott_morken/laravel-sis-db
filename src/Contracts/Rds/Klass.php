<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 1:16 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;
use Smorken\SisDb\Contracts\Cds\Instructor;
use Smorken\SisDb\Contracts\Cds\Meeting;

/**
 * Interface Klass
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $acad_career
 * @property $acad_org
 * @property $acad_org_desc
 * @property $catalog_number
 * @property $class_name
 * @property $class_number
 * @property $college_id
 * @property $combined_id
 * @property $combined_list
 * @property $combined_major
 * @property $combined_section
 * @property $component_code
 * @property $component_desc
 * @property $consent
 * @property $consent_desc
 * @property $course_id
 * @property $course_offer_number
 * @property $day_evening_code
 * @property $day_evening_desc
 * @property $department_id
 * @property $description
 * @property $dual_enroll
 * @property $dual_enroll_sponsor
 * @property $enrolled_cap
 * @property $enrollment_status
 * @property $enrollment_status_desc
 * @property $grading_basis
 * @property $honors
 * @property $instruction_mode
 * @property $instruction_mode_desc
 * @property $location_code
 * @property $location_desc
 * @property $major_class_name
 * @property $major_class_number
 * @property $major_class
 * @property $primary_instructor_id
 * @property $primary_instructor_name
 * @property $rollover
 * @property $schedule_print
 * @property $section
 * @property $session_code
 * @property $session_type
 * @property $status
 * @property $status_desc
 * @property $subject
 * @property $term_id
 * @property $units
 * @property Carbon $end_date
 * @property Carbon $start_date
 *
 * @property Material[] $materials
 * @property Enrollment[] $enrollments
 * @property Meeting[] $meetings
 * @property Instructor[] $instructors
 */
interface Klass
{

}
