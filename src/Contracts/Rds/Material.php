<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/2/16
 * Time: 8:03 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Material
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $sequence_number
 * @property $type_code
 * @property $type_desc
 * @property $status_code
 * @property $status_desc
 * @property $title
 * @property $isbn
 * @property $author
 * @property $publisher
 * @property $edition
 * @property $year_published
 * @property $price
 * @property $notes
 */
interface Material
{

}
