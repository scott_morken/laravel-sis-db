<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:06 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Person
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $id
 * @property $alt_id
 * @property $first_name
 * @property $last_name
 * @property $email
 * @property $dob
 * @property $hrms_id
 */
interface Person
{

    public function shortName();

    public function fullName();
}
