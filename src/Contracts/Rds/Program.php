<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/25/16
 * Time: 10:56 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Program
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $student_id
 * @property $college_id
 * @property $program_type
 * @property $program_status
 * @property $program_last_action
 * @property $admit_term
 * @property $plan_name
 * @property $plan_type
 * @property $award_type
 * @property $grad_term
 * @property $grad_status_code
 * @property $grad_status_desc
 * @property $interest
 *
 * @property Student $student
 */
interface Program
{

}
