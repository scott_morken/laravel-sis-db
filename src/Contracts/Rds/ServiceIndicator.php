<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/9/16
 * Time: 12:21 PM
 */

namespace Smorken\SisDb\Contracts\Rds;

use Carbon\Carbon;

/**
 * Interface ServiceIndicator
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $student_id
 * @property Carbon $svc_datetime
 * @property $entered_by
 * @property $college_id
 * @property $code
 * @property $description
 * @property $reason_code
 * @property $reason_desc
 * @property $term_Id
 * @property Carbon $active_date
 * @property $positive
 * @property $amount
 * @property $comments
 * @property $contact
 * @property $department
 *
 * @property Student $student
 */
interface ServiceIndicator
{

}
