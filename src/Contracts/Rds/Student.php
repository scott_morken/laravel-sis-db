<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:08 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Student
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $id
 * @property $alt_id
 * @property $first_name
 * @property $last_name
 * @property $email
 *
 * @property Enrollment[] $enrollments
 * @property StudentTerm[] $studentTerms
 * @property Group[] $groups
 */
interface Student extends Person
{

    /**
     * @param string $group
     * @return bool
     */
    public function memberOf($group);
}
