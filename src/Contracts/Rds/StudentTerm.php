<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/2/15
 * Time: 11:53 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface StudentTerm
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $student_id
 * @property $term_id
 * @property $cumu_gpa
 * @property $term_gpa
 * @property $cum_attempted_hours;
 * @property $cumu_earned_hours;
 * @property $cumu_gpa_hours;
 * @property $cumu_gpa_points;
 * @property $term_attempted_hours;
 * @property $term_earned_hours;
 * @property $term_gpa_hours;
 * @property $term_gpa_points;
 * @property $academic_standing;
 * @property $load
 * @property $inprogress_gpa
 * @property $inprogress_nogpa
 *
 * @property Student $student
 */
interface StudentTerm
{

}
