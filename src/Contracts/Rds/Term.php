<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 7:09 AM
 */

namespace Smorken\SisDb\Contracts\Rds;

/**
 * Interface Term
 * @package Smorken\SisDb\Contracts\Rds
 *
 * @property $id
 * @property $full_id
 * @property $description
 * @property $start_date
 * @property $end_date
 * @property array $college_ids
 * @property $college_id
 */
interface Term
{

}
