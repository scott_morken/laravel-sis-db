<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/15/17
 * Time: 12:26 PM
 */

namespace Smorken\SisDb\Contracts\VO;

/**
 * Interface AcademicYear
 * @package Smorken\SisDb\Contracts\VO
 *
 * @property $id
 * @property $description
 * @property $description_long
 * @property $start_date
 * @property $end_date
 */
interface AcademicYear
{

}
