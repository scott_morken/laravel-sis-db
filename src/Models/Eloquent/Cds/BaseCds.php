<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 10:40 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Smorken\Model\Convertible\Eloquent\Model;
use Smorken\SisDb\Models\Eloquent\Traits\TermLike;

abstract class BaseCds extends Model
{

    use TermLike;

    protected $connection = 'sis';
}
