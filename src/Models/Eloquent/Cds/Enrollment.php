<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 9:22 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Smorken\SisDb\Models\Eloquent\Rds\Term;

class Enrollment extends BaseCds implements \Smorken\SisDb\Contracts\Cds\Enrollment
{

    protected $table = 'CDS_STU_CLASS_TBL';

    protected $columns = [
        'ENRL_ACAD_CAREER',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_CLASS_NBR',
        'ENRL_EMPLID',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_GRADING_BASIS',
        'ENRL_INSTITUTION_CD',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_STATUS_REASON_CD',
        'ENRL_TERM_CD',
        'ENRL_UNITS_TAKEN',
    ];

    protected $relation_map = [
        'klass'   => Klass::class,
        'student' => Student::class,
        'term'    => Term::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'       => 'ENRL_EMPLID',
                'college_id'       => 'ENRL_INSTITUTION_CD',
                'term_id'          => 'ENRL_TERM_CD',
                'class_number'     => 'ENRL_CLASS_NBR',
                'acad_career'      => 'ENRL_ACAD_CAREER',
                'grading_basis'    => 'ENRL_GRADING_BASIS',
                'grade'            => 'ENRL_OFFICIAL_GRADE',
                'grade_date'       => 'ENRL_GRADE_DATE',
                'units_taken'      => 'ENRL_UNITS_TAKEN',
                'grade_points'     => 'ENRL_GRADE_POINTS',
                'status_code'      => 'ENRL_STATUS_REASON_CD',
                'status_desc'      => function ($attr, $self) {
                    return $self->getStatusDesc();
                },
                'last_action_code' => 'ENRL_ACTN_RSN_LAST_CD',
                'last_action_desc' => 'ENRL_ACTN_RSN_LAST_LDESC',
            ],
        ];
    }

    public function getStatusDesc()
    {
        $s = [
            'CANC' => 'Dropped (cancelled)',
            'DROP' => 'Dropped (enrolled)',
            'ENRL' => 'Enrolled',
            'EWAT' => 'Enrolled from wait list',
            'FULL' => 'Section full',
            'RCMP' => 'Related component',
            'WDRW' => 'Withdrawn',
        ];
        return $this->getFromArray($s, 'ENRL_STATUS_REASON_CD');
    }

    public function klass()
    {
        $klass = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $klass,
            ['ENRL_CLASS_NBR', 'ENRL_TERM_CD'],
            ['CLASS_CLASS_NBR', 'CLASS_TERM_CD']
        )
                    ->defaultColumns();
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasOne($rel, 'ENRL_EMPLID', 'PERS_EMPLID')
                    ->defaultColumns();
    }

    public function term()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            ['ENRL_TERM_CD', 'ENRL_INSTITUTION_CD', 'ENRL_ACAD_CAREER'],
            ['STRM', 'INSTITUTION', 'ACAD_CAREER']
        )
                    ->defaultColumns();
    }

    public function getENRLGRADEDATEAttribute()
    {
        return $this->toCarbonDate('ENRL_GRADE_DATE');
    }

    public function scopeByTermId($query, $term_id)
    {
        return $this->whereTermLike('ENRL_TERM_CD', $query, $term_id);
    }

    public function scopeByCollegeId($query, $college_id)
    {
        return $this->simpleWhere($query, 'ENRL_INSTITUTION_CD', $college_id);
    }

    public function scopeByStudentId($query, $student_id)
    {
        return $this->simpleWhere($query, 'ENRL_EMPLID', $student_id);
    }

    public function scopeEnrolled($query)
    {
        $query->whereIn('ENRL_STATUS_REASON_CD', ['ENRL', 'EWAT']);
        return $query;
    }

    public function scopeGraded($query)
    {
        $query->whereNotNull('ENRL_GRADE_DATE');
        return $query;
    }

    public function scopeCredit($query)
    {
        $query->where('ENRL_ACAD_CAREER', '=', 'CRED');
        return $query;
    }

    public function scopeActiveKlass($query)
    {
        $query->whereHas(
            'klass',
            function ($q) {
                $q->whereNull('CLASS_CANCEL_DATE');
            }
        );
        return $query;
    }

    public function scopeActiveAndUpcomingTerm($query)
    {
        return $this->scopeTermEndingAfter($query, date('Y-m-d', strtotime('-1 day')));
    }

    public function scopeTermEndingAfter($query, $date = '-3 years')
    {
        $date = date('Y-m-d', strtotime($date));
        $query->whereHas(
            'term',
            function ($q) use ($date) {
                $q->whereDate('TERM_END_DT', '>=', $date);
            }
        );
        return $query;
    }

    public function scopeOrderCollegeTermNumber($query)
    {
        $query->orderBy('ENRL_INSTITUTION_CD')
              ->orderBy('ENRL_TERM_CD')
              ->orderBy('ENRL_CLASS_NBR');
        return $query;
    }
}
