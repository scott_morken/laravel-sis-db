<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:28 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

class Instructor extends Person implements \Smorken\SisDb\Contracts\Cds\Instructor
{

    protected $relation_map = [
        'instructsKlasses' => 'Smorken\SisDb\Models\Eloquent\Cds\KlassInstructor',
    ];

    public function instructsKlasses()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'CLASSM_INSTRUCTOR_EMPLID', 'PERS_EMPLID');
    }
}
