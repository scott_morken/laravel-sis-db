<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:52 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Illuminate\Database\Query\Expression;
use Smorken\Ext\Database\Eloquent\ExpressionValue;
use Smorken\SisDb\Models\Eloquent\Rds\Fee;

class Klass extends BaseCds implements \Smorken\SisDb\Contracts\Cds\Klass
{

    use \Smorken\SisDb\Models\Eloquent\Traits\Klass;

    protected $table = 'CDS_CLASS_TBL';

    protected $columns = [
        'CASSC_GRADING_BASIS',
        'CASSC_HONORS_YN',
        'CASSC_UNITS_MINIMUM',
        'CLASS_ACAD_CAREER',
        'CLASS_ACAD_ORG',
        'CLASS_ACAD_ORG_LDESC',
        'CLASS_CATALOG_NBR',
        'CLASS_CLASS_NAME',
        'CLASS_CLASS_NBR',
        'CLASS_CLASS_STAT',
        'CLASS_COMPONENT_CD',
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_DAY_EVE',
        'CLASS_DESCR',
        'CLASS_END_DATE',
        'CLASS_ENRL_CAP',
        'CLASS_ENRL_STAT',
        'CLASS_ENRL_TOT',
        'CLASS_INSTITUTION_CD',
        'CLASS_INSTRUCTION_MODE',
        'CLASS_LOCATION_CD',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_START_DATE',
        'CLASS_SUBJECT_CD',
        'CLASS_TERM_CD',
    ];

    protected $relation_map = [
        'materials'   => \Smorken\SisDb\Models\Eloquent\Rds\Material::class,
        'enrollments' => \Smorken\SisDb\Models\Eloquent\Cds\Enrollment::class,
        'meetings'    => \Smorken\SisDb\Models\Eloquent\Cds\Meeting::class,
        'instructors' => \Smorken\SisDb\Models\Eloquent\Cds\KlassInstructor::class,
        'term'        => \Smorken\SisDb\Models\Eloquent\Rds\Term::class,
        'klassNotes'  => KlassNote::class,
        'courseNotes' => CourseNote::class,
        'fees'        => Fee::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'acad_career'            => 'CLASS_ACAD_CAREER',
                'acad_org'               => 'CLASS_ACAD_ORG',
                'acad_org_desc'          => 'CLASS_ACAD_ORG_LDESC',
                'campus_code'            => 'CLASS_CAMPUS_CD',
                'catalog_number'         => 'CLASS_CATALOG_NBR',
                'class_name'             => 'CLASS_CLASS_NAME',
                'class_number'           => 'CLASS_CLASS_NBR',
                'college_id'             => 'CLASS_INSTITUTION_CD',
                'component_code'         => 'CLASS_COMPONENT_CD',
                'course_id'              => 'CLASS_COURSE_ID',
                'course_offer_number'    => 'CLASS_COURSE_OFFER_NBR',
                'day_evening_code'       => 'CLASS_DAY_EVE',
                'description'            => 'CLASS_DESCR',
                'end_date'               => function ($attr, $self) {
                    return $this->toCarbonDate('CLASS_END_DATE');
                },
                'enrolled'               => 'CLASS_ENRL_TOT',
                'enrolled_cap'           => 'CLASS_ENRL_CAP',
                'enrollment_status'      => 'CLASS_ENRL_STAT',
                'grading_basis'          => 'CASSC_GRADING_BASIS',
                'instruction_mode'       => 'CLASS_INSTRUCTION_MODE',
                'location_code'          => 'CLASS_LOCATION_CD',
                'section'                => 'CLASS_SECTION',
                'session_code'           => 'CLASS_SESSION_CD',
                'start_date'             => function ($attr, $self) {
                    return $this->toCarbonDate('CLASS_START_DATE');
                },
                'status'                 => 'CLASS_CLASS_STAT',
                'subject'                => 'CLASS_SUBJECT_CD',
                'term_id'                => 'CLASS_TERM_CD',
                'units'                  => 'CASSC_UNITS_MINIMUM',
                'component_desc'         => function ($attr, $self) {
                    return $this->getComponentDesc();
                },
                'day_evening_desc'       => function ($attr, $self) {
                    return $this->getDayEveDesc();
                },
                'enrollment_status_desc' => function ($attr, $self) {
                    return $this->getEnrollmentStatusDesc();
                },
                'honors'                 => function ($attr, $self) {
                    $honors = $this->arrayGet($this->attributes, 'CASSC_HONORS_YN');
                    return $honors === 'Y' ? 1 : 0;
                },
                'instruction_mode_desc'  => function ($attr, $self) {
                    return $this->getInstructionModeDesc();
                },
                'status_desc'            => function ($attr, $self) {
                    return $this->getStatusDesc();
                },
            ],
        ];
    }

    public function getComponentDesc()
    {
        $comps = [
            'LAB' => 'Laboratory',
            'L+L' => 'Lecture & Lab',
            'LEC' => 'Lecture',
        ];
        return $this->getFromArray($comps, 'CLASS_COMPONENT_CD');
    }

    public function getDayEveDesc()
    {
        $de = [
            'D' => 'Day',
            'E' => 'Evening',
        ];
        return $this->getFromArray($de, 'CLASS_DAY_EVE');
    }

    public function getEnrollmentStatusDesc()
    {
        $s = [
            'O' => 'Open',
            'C' => 'Closed',
        ];
        return $this->getFromArray($s, 'CLASS_ENRL_STAT');
    }

    public function getInstructionModeDesc()
    {
        $i = [
            'P'  => 'In Person',
            'IN' => 'Internet',
            'HY' => 'Hybrid',
            'FB' => 'Field Based',
            'IS' => 'Independent Study',
            'PI' => 'Private Instruction',
            'SA' => 'Study Abroad',
        ];
        return $this->getFromArray($i, 'CLASS_INSTRUCTION_MODE');
    }

    public function getStatusDesc()
    {
        $s = [
            'X' => 'Cancelled Section',
            'A' => 'Active',
            'T' => 'Tentative Section',
            'S' => 'Stop Further Enrollment',
        ];
        return $this->getFromArray($s, 'CLASS_CLASS_STAT');
    }

    public function materials()
    {
        $outerfunc = function ($start, $length) {
            return function ($ev, $model) use ($start, $length) {
                return substr($model->TBMAT_SID, $start, $length);
            };
        };
        $exp1 = new Expression('SUBSTRING(CAST(TBMAT_SID as varchar(9)), 1, 4)');
        $exp2 = new Expression('SUBSTRING(CAST(TBMAT_SID as varchar(9)), 5, 5)');
        $ve1 = new ExpressionValue('TBMAT_SID', $exp1, $outerfunc(0, 4));
        $ve2 = new ExpressionValue('TBMAT_SID', $exp2, $outerfunc(4, 5));
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                $ve1,
                $ve2,
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns();
    }

    public function enrollments()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'ENRL_TERM_CD',
                'ENRL_CLASS_NBR',
            ]
        )
                    ->defaultColumns();
    }
}
