<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:29 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

class KlassInstructor extends BaseCds implements \Smorken\SisDb\Contracts\Cds\KlassInstructor
{

    protected $table = 'CDS_CLASS_INSTR_TBL';

    protected $columns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_INSTITUTION_CD',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'CLASSM_ASSIGN_TYPE',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_INSTR_ASSIGN_SEQ',
        'CLASSM_INSTR_LOAD_FACTOR',
        'CLASSM_INSTR_NAME_FLAST',
        'CLASSM_INSTR_TYPE',
        'CLASSM_INSTRUCTOR_EMPLID',
        'CLASSM_INSTRUCTOR_NAME',
        'CLASSM_INSTRUCTOR_ROLE',
    ];

    protected $relation_map = [
        'klass'      => Klass::class,
        'meetings'   => Meeting::class,
        'instructor' => Instructor::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'sequence_number'      => 'CLASSM_INSTR_ASSIGN_SEQ',
                'instructor_id'        => 'CLASSM_INSTRUCTOR_EMPLID',
                'full_name'                 => 'CLASSM_INSTRUCTOR_NAME',
                'short_name'           => 'CLASSM_INSTR_NAME_FLAST',
                'type_code'            => 'CLASSM_INSTR_TYPE',
                'type_desc'            => function ($attr, $self) {
                    return $this->getTypeDesc();
                },
                'role_code'            => 'CLASSM_INSTRUCTOR_ROLE',
                'role_desc'            => function ($attr, $self) {
                    return $this->getRoleDesc();
                },
                'load'                 => 'CLASSM_INSTR_LOAD_FACTOR',
                'assignment_type_code' => 'CLASSM_ASSIGN_TYPE',
                'college_id'           => 'CLASS_INSTITUTION_CD',
            ],
        ];
    }

    public function getTypeDesc()
    {
        $types = [
            'ADJ' => 'Adjunct',
            'RES' => 'Residential',
        ];
        return $this->getFromArray($types, $this->type_code);
    }

    public function getRoleDesc()
    {
        $roles = [
            'PI' => 'Primary',
            'SI' => 'Secondary',
        ];
        return $this->getFromArray($roles, $this->role_code);
    }

    public function klass()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function meetings()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
                    ->defaultColumns();
    }

    public function instructor()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'CLASSM_INSTRUCTOR_EMPLID', 'PERS_EMPLID')
                    ->defaultColumns();
    }

    public function scopePrimary($query)
    {
        return $this->scopeRole($query, 'PI');
    }

    public function scopeRole($query, $role = 'PI')
    {
        $query->where('CLASSM_INSTRUCTOR_ROLE', '=', $role);
        return $query;
    }

    public function scopeSecondary($query)
    {
        return $this->scopeRole($query, 'SI');
    }

    public function scopeResidential($query)
    {
        return $this->scopeType($query, 'RES');
    }

    public function scopeType($query, $type = 'RES')
    {
        $query->where('CLASSM_INSTR_TYPE', '=', $type);
        return $query;
    }

    public function scopeAdjunct($query)
    {
        return $this->scopeType($query, 'ADJ');
    }
}
