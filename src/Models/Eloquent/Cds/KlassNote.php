<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/1/17
 * Time: 8:22 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

class KlassNote extends BaseCds implements \Smorken\SisDb\Contracts\Cds\KlassNote
{

    protected $table = 'CDS_CLASS_NOTES_TBL';

    protected $columns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'NOTES_SEQ_NBR',
        'NOTES_NOTE_NBR',
        'NOTES_TYPE_PUBL_STAF',
        'NOTES_DESCR',
        'NOTES_NOTE_TEXT',
        'CLASS_INSTITUTION_CD',
    ];

    protected $relation_map = [
        'klass' => Klass::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'sequence_number' => 'NOTES_SEQ_NBR',
                'note_number'     => 'NOTES_NOTE_NBR',
                'college_id'      => 'CLASS_INSTITUTION_CD',
                'type'            => 'NOTES_TYPE_PUBL_STAF',
                'description'     => 'NOTES_DESCR',
                'text'            => 'NOTES_NOTE_TEXT',
            ],
        ];
    }
}
