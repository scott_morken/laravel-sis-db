<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:38 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Carbon\Carbon;

class Meeting extends BaseCds implements \Smorken\SisDb\Contracts\Cds\Meeting
{

    protected $table = 'CDS_CLASS_MTG_TBL';

    protected $columns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'CLASSM_BLDG_CD',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_DAYS_PATTERN',
        'CLASSM_END_DATE',
        'CLASSM_FACILITY_ID',
        'CLASSM_LOCATION',
        'CLASSM_MEETING_TIME_END',
        'CLASSM_MEETING_TIME_START',
        'CLASSM_ROOM',
        'CLASSM_START_DATE',
    ];

    protected $relation_map = [
        'klass'       => Klass::class,
        'instructors' => KlassInstructor::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'meeting_id'    => 'CLASSM_CLASS_MTG_NBR',
                'college_id'    => 'CLASS_INSTITUTION_CD',
                'building_code' => 'CLASSM_BLDG_CD',
                'location_code' => 'CLASSM_LOCATION',
                'room'          => 'CLASSM_FACILITY_ID',
                'start_date'    => function ($attr, $self) {
                    return $this->toCarbonDate('CLASSM_START_DATE');
                },
                'end_date'      => function ($attr, $self) {
                    return $this->toCarbonDate('CLASSM_END_DATE');
                },
                'start_time'    => function ($attr, $self) {
                    return $this->toCarbonDate('CLASSM_MEETING_TIME_START');
                },
                'end_time'      => function ($attr, $self) {
                    return $this->toCarbonDate('CLASSM_MEETING_TIME_END');
                },
                'days'          => function ($attr, $self) {
                    return $this->getMeetingDays();
                },
            ],
        ];
    }

    public function getMeetingDays()
    {
        $str = $this->arrayGet($this->attributes, 'CLASSM_DAYS_PATTERN', null);
        $conversion = [
            'U' => Carbon::SUNDAY,
            'M' => Carbon::MONDAY,
            'T' => Carbon::TUESDAY,
            'W' => Carbon::WEDNESDAY,
            'R' => Carbon::THURSDAY,
            'F' => Carbon::FRIDAY,
            'S' => Carbon::SATURDAY,
        ];
        $days = trim($str);
        $pattern = [];
        foreach (str_split($days) as $d) {
            if ($d) {
                $pattern[] = $conversion[$d];
            }
        }
        return $pattern;
    }

    public function klass()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function instructors()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
                    ->defaultColumns();
    }

    public function primaryInstructor()
    {
        $primary = null;
        $instrs = $this->instructors;
        foreach ($instrs as $inst) {
            if ($primary === null) {
                $primary = $inst;
            }
            if ($inst->role_code === 'PI' && $primary->role_code !== 'PI') {
                $primary = $inst;
            }
            if ($inst->load > $primary->load && $inst->role_code === 'PI') {
                $primary = $inst;
            }
        }
        return $primary;
    }

    public function daysAsString($long = false)
    {
        $strArr = [];
        $conv = [
            Carbon::SUNDAY    => ['Sun', 'U'],
            Carbon::MONDAY    => ['Mon', 'M'],
            Carbon::TUESDAY   => ['Tue', 'T'],
            Carbon::WEDNESDAY => ['Wed', 'W'],
            Carbon::THURSDAY  => ['Thu', 'R'],
            Carbon::FRIDAY    => ['Fri', 'F'],
            Carbon::SATURDAY  => ['Sat', 'S'],
        ];
        $key = $long ? 0 : 1;
        if ($this->days) {
            foreach ($this->days as $d) {
                if ($d) {
                    $strArr[] = $conv[$d][$key];
                }
            }
        }
        return implode($long ? ' ' : '', $strArr);
    }

    public function getCLASSMSTARTDATEAttribute($k)
    {
        return $this->toCarbonDate('CLASSM_START_DATE');
    }

    public function getCLASSMENDDATEAttribute($k)
    {
        return $this->toCarbonDate('CLASSM_END_DATE');
    }

    public function getCLASSMMEETINGTIMESTARTAttribute($k)
    {
        return $this->toCarbonDate('CLASSM_MEETING_TIME_START');
    }

    public function getCLASSMMEETINGTIMEENDAttribute($k)
    {
        return $this->toCarbonDate('CLASSM_MEETING_TIME_END');
    }
}
