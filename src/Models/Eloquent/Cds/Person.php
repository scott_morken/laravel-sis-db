<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:16 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Smorken\Convertible\Models\VO;

class Person extends BaseCds implements \Smorken\SisDb\Contracts\Cds\Person
{

    protected $table = 'CDS_PERSON_TBL';

    /**
     * @var VO
     */
    protected $groups;

    protected $columns = [
        'PERS_EMPLID',
        'PERS_OPRID',
        'PERS_PRIMARY_FIRST_NAME',
        'PERS_PRIMARY_LAST_NAME',
        'PERS_EMAIL_ADDR',
        'PERS_BIRTHDATE',
        'HRMS_EMPLID',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'id'         => 'PERS_EMPLID',
                'alt_id'     => 'PERS_OPRID',
                'first_name' => 'PERS_PRIMARY_FIRST_NAME',
                'last_name'  => 'PERS_PRIMARY_LAST_NAME',
                'email'      => 'PERS_EMAIL_ADDR',
                'dob'        => 'PERS_BIRTHDATE',
                'hrms_id'    => 'HRMS_EMPLID',
                'groups'     => function ($attr, $self) {
                    return $self->getGroups();
                },
            ],
        ];
    }

    public function getPERSBIRTHDATEAttribute()
    {
        return $this->toCarbonDate('PERS_BIRTHDATE');
    }

    public function name()
    {
        return $this->fullName();
    }

    public function fullName()
    {
        return sprintf('%s %s', $this->getFirstName(null), $this->getLastName(null));
    }

    public function getFirstName($v)
    {
        return trim(
            $this->PERS_PREFERRED_FIRST_NAME
        ) ? $this->PERS_PREFERRED_FIRST_NAME : $this->PERS_PRIMARY_FIRST_NAME;
    }

    public function getLastName($v)
    {
        return trim($this->PERS_PREFERRED_LAST_NAME) ? $this->PERS_PREFERRED_LAST_NAME : $this->PERS_PRIMARY_LAST_NAME;
    }

    public function shortName()
    {
        return sprintf('%s. %s', substr($this->getFirstName(null), 0, 1), $this->getLastName(null));
    }

    public function getGroups()
    {
        if (!$this->groups) {
            $this->groups = new VO(
                [
                    'veteran' => $this->arrayGet($this->attributes, 'SIF_MILITARY_VETERAN') === 'Y',
                    'ace'     => false,
                    'trio'    => false,
                ]
            );
        }
        return $this->groups;
    }

    /**
     * @param string $group
     * @return bool
     */
    public function memberOf($group)
    {
        // TODO: Implement memberOf() method.
    }
}
