<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:28 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Smorken\SisDb\Models\Eloquent\Rds\Group;

class Student extends Person implements \Smorken\SisDb\Contracts\Cds\Student
{

    protected $relation_map = [
        'enrollments'  => Enrollment::class,
        'studentTerms' => StudentTerm::class,
        'groups'       => Group::class,
    ];

    public function memberOf($group)
    {
        foreach ($this->groups as $g) {
            if ($g->id === $group) {
                return true;
            }
        }
        return false;
    }

    public function enrollments()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'ENRL_EMPLID',
            'PERS_EMPLID'
        )
                    ->defaultColumns();
    }

    public function studentTerms()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'CTERM_EMPLID',
            'PERS_EMPLID'
        )
                    ->defaultColumns();
    }

    public function groups()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'STDGR_EMPLID',
            'PERS_EMPLID'
        )
                    ->defaultColumns()
                    ->active();
    }
}
