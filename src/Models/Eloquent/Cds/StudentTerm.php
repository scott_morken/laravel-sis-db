<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 8:10 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Cds;

use Smorken\SisDb\Models\Eloquent\Rds\Term;

class StudentTerm extends BaseCds implements \Smorken\SisDb\Contracts\Cds\StudentTerm
{

    protected $table = 'CDS_STU_TERM_TBL';

    protected $columns = [
        'CTERM_EMPLID',
        'CTERM_TERM_CD',
        'CTERM_INSTITUTION_CODE',
        'CTERM_ACAD_CAREER',
        'STFACT_MCCD_TERM_AHRS',
        'STFACT_MCCD_TERM_EHRS',
        'STFACT_MCCD_TERM_QHRS',
        'STFACT_MCCD_TERM_QPTS',
        'STFACT_MCCD_CUM_AHRS',
        'STFACT_MCCD_CUM_EHRS',
        'STFACT_MCCD_CUM_QHRS',
        'STFACT_MCCD_CUM_QPTS',
        'CTERM_ACAD_STNDNG_ACTN',
        'CTERM_ACAD_LOAD_CODE',
    ];

    protected $relation_map = [
        'student' => Student::class,
        'term'    => Term::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'             => 'CTERM_EMPLID',
                'term_id'                => 'CTERM_TERM_CD',
                'college_id'             => 'CTERM_INSTITUTION_CODE',
                'acad_career'            => 'CTERM_ACAD_CAREER',
                'cumu_gpa'               => function ($attr, $self) {
                    return $self->getCumuGpa();
                },
                'term_gpa'               => function ($attr, $self) {
                    return $self->getTermGpa();
                },
                'cumu_attempted_hours'   => 'STFACT_MCCD_CUM_AHRS',
                'cumu_earned_hours'      => 'STFACT_MCCD_CUM_EHRS',
                'cumu_gpa_hours'         => 'STFACT_MCCD_CUM_QHRS',
                'cumu_gpa_points'        => 'STFACT_MCCD_CUM_QPTS',
                'term_attempted_hours'   => 'STFACT_MCCD_TERM_AHRS',
                'term_earned_hours'      => 'STFACT_MCCD_TERM_EHRS',
                'term_gpa_hours'         => 'STFACT_MCCD_TERM_QHRS',
                'term_gpa_points'        => 'STFACT_MCCD_TERM_QPTS',
                'academic_standing_code' => 'CTERM_ACAD_STNDNG_ACTN',
                'academic_standing_desc' => function ($attr, $self) {
                },
                'load_code'              => 'CTERM_ACAD_LOAD_CODE',
                'load_desc'              => function ($attr, $self) {
                    return $self->getLoadDesc();
                },
            ],
        ];
    }

    public function term()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            ['CTERM_TERM_CD', 'CTERM_INSTITUTION_CODE', 'CTERM_ACAD_CAREER'],
            ['STRM', 'INSTITUTION', 'ACAD_CAREER']
        )
                    ->defaultColumns();
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo(
            $rel,
            'PERS_EMPLID',
            'CTERM_EMPLID'
        )
                    ->defaultColumns();
    }

    public function getCumuGpa()
    {
        $qhrs = $this->arrayGet($this->attributes, 'STFACT_MCCD_CUM_QHRS', 0);
        $qpts = $this->arrayGet($this->attributes, 'STFACT_MCCD_CUM_QPTS', 0);
        return $this->getGpa($qhrs, $qpts);
    }

    protected function getGpa($hours, $points)
    {
        if ($hours > 0) {
            return number_format($points / $hours, 3);
        }
        return false;
    }

    public function getTermGpa()
    {
        $qhrs = $this->arrayGet($this->attributes, 'STFACT_MCCD_TERM_QHRS', 0);
        $qpts = $this->arrayGet($this->attributes, 'STFACT_MCCD_TERM_QPTS', 0);
        return $this->getGpa($qhrs, $qpts);
    }

    public function getLoadDesc()
    {
        $l = [
            'N' => 'No Load',
            'L' => 'Less than Half',
            'H' => 'Half-Time',
            'T' => 'Three Quarter Time',
            'F' => 'Full-Time',
        ];
        return $this->getFromArray($l, 'CTERM_ACAD_LOAD_CODE');
    }

    public function getStandingDesc()
    {
        $s = [
            'ACPT' => 'Acceptable',
            'CONT' => 'Continued Probation',
            'PROB' => 'Academic Probation',
        ];
        return $this->getFromArray($s, 'CTERM_ACAD_STNDNG_ACTN');
    }

    public function scopeOrderReverse($query)
    {
        $query->orderBy('CTERM_INSTITUTION_CODE');
        $query->orderBy('CTERM_TERM_CD', 'desc');
        $query->orderBy('CTERM_EMPLID');
        return $query;
    }

    public function scopeCredit($query)
    {
        $query->where('CTERM_ACAD_CAREER', '<>', 'NC');
        return $query;
    }

    public function scopeLoad($query)
    {
        $query->where('CTERM_ACAD_LOAD_CODE', '<>', 'N');
        return $query;
    }

    public function scopeLoadAndCredit($query)
    {
        $query->credit()->load();
        return $query;
    }

    public function scopeLessThanEqualTerm($query, $term)
    {
        $query->where(\Db::raw('SUBSTRING(CTERM_TERM_CD, 2, 3)'), '<=', $term);
        return $query;
    }

    public function scopeOrder($query)
    {
        $query->orderBy('CTERM_INSTITUTION_CODE');
        $query->orderBy('CTERM_TERM_CD');
        $query->orderBy('CTERM_EMPLID');
        return $query;
    }

    public function scopeByCollegeId($query, $college_id = null)
    {
        return $this->simpleWhere($query, 'CTERM_INSTITUTION_CODE', $college_id);
    }

    public function scopeByStudentId($query, $student_id)
    {
        return $this->simpleWhere($query, 'CTERM_EMPLID', $student_id);
    }

    public function scopeByTermId($query, $term_id)
    {
        return $this->whereTermLike('CTERM_TERM_CD', $query, $term_id);
    }

    public function scopeGreaterThanEqualCredits($query, $credits)
    {
        $query->where('STFACT_MCCD_CUM_EHRS', '>=', $credits);
        return $query;
    }

    public function scopeGraded($query)
    {
        $query->where(
            function ($qs) {
                $qs->where('STFACT_MCCD_TERM_EHRS', '>', 0)
                   ->orWhere('STFACT_MCCD_TERM_QHRS', '>', 0);
            }
        );
        return $query;
    }

    public function scopeActive($query)
    {
        $query->where('STFACT_MCCD_TERM_AHRS', '>', 0);
        return $query;
    }

    public function scopeGreaterThanEqualLoad($query, $load)
    {
        $mixes = [
            'N' => ['N', 'L', 'H', 'T', 'F'],
            'L' => ['L', 'H', 'T', 'F'],
            'H' => ['H', 'T', 'F'],
            'T' => ['T', 'F'],
            'F' => ['F'],
        ];
        $load = $load ?: 'L';
        $query->whereIn('CTERM_ACAD_LOAD_CODE', $mixes[$load]);
        return $query;
    }
}
