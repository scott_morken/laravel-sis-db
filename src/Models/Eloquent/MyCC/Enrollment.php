<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:47 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

use Smorken\Model\Relations\Traits\FakeRelations;

class Enrollment extends \Smorken\SisDb\Models\Eloquent\Cds\Enrollment implements \Smorken\SisDb\Contracts\Cds\Enrollment
{

    use FakeRelations;

    protected $table = 'MYCC_STU_CLASS_VW';

    protected $columns = [
        'CTERM_INST_ABBREV',
        'CTERM_INST_DESCR50',
        'ENRL_ACAD_CAREER',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_CLASS_NBR',
        'ENRL_EMPLID',
        'ENRL_ENRL_ADD_DATE',
        'ENRL_ENRL_DROP_DATE',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_GRADING_BASIS',
        'ENRL_INSTITUTION_CD',
        'ENRL_OEE_END_DATE',
        'ENRL_OEE_START_DATE',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_STATUS_REASON_CD',
        'ENRL_TERM_BEGIN_DATE',
        'ENRL_TERM_CD',
        'ENRL_TERM_DESCR',
        'ENRL_TERM_END_DATE',
        'ENRL_UNITS_TAKEN',
    ];

    protected $relation_map = [
        'klass'   => Klass::class,
        'student' => Student::class,
        'term'    => Term::class,
        'college' => College::class,
    ];

    public function college()
    {
        $college = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $college,
            ['id' => 'ENRL_INSTITUTION_CD', 'description' => 'CTERM_INST_DESCR50', 'abbrev' => 'CTERM_INST_ABBREV']
        );
    }

    public function term()
    {
        $term = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $term,
            [
                'STRM'          => 'ENRL_TERM_CD',
                'DESCR'         => 'ENRL_TERM_DESCR',
                'ACAD_CAREER'   => 'ENRL_ACAD_CAREER',
                'TERM_BEGIN_DT' => 'ENRL_TERM_BEGIN_DATE',
                'TERM_END_DT'   => 'ENRL_TERM_END_DATE',
                'INSTITUTION'   => 'ENRL_INSTITUTION_CD',
            ]
        );
    }

    public function scopeTermEndingAfter($query, $date = '-3 years')
    {
        $date = date('Y-m-d', strtotime($date));
        $query->where('ENRL_TERM_END_DATE', '>=', $date);
        return $query;
    }

    public function scopeCurrentAndFutureClasses($query)
    {
        $query->whereHas(
            'klass',
            function ($q) {
                $q->where('CLASS_END_DATE', '>=', date('Y-m-d'));
            }
        );
        return $query;
    }
}
