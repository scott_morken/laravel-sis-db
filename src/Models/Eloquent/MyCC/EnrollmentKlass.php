<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:57 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

use Smorken\Model\Relations\Traits\FakeRelations;

class EnrollmentKlass extends Enrollment implements \Smorken\SisDb\Contracts\Cds\Enrollment
{

    use FakeRelations;

    protected $table = 'MYCC_ENROLLED_CLASSES_VW';

    protected $columns = [
        'CASSC_GRADING_BASIS',
        'CASSC_HONORS_YN',
        'CASSC_UNITS_MINIMUM',
        'CLASS_ACAD_CAREER',
        'CLASS_ACAD_ORG',
        'CLASS_ACAD_ORG_LDESC',
        'CLASS_CATALOG_NBR',
        'CLASS_CLASS_NAME',
        'CLASS_CLASS_NBR',
        'CLASS_CLASS_STAT',
        'CLASS_COMPONENT_CD',
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_DAY_EVE',
        'CLASS_DESCR',
        'CLASS_END_DATE',
        'CLASS_ENRL_CAP',
        'CLASS_ENRL_STAT',
        'CLASS_ENRL_TOT',
        'CLASS_INSTITUTION_CD',
        'CLASS_INSTRUCTION_MODE',
        'CLASS_LOCATION_CD',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_START_DATE',
        'CLASS_SUBJECT_CD',
        'CLASS_TERM_CD',
        'ENRL_ACAD_CAREER',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_CLASS_NBR',
        'ENRL_EMPLID',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_GRADING_BASIS',
        'ENRL_INST_ABBREV',
        'ENRL_INST_DESCR50',
        'ENRL_INSTITUTION_CD',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_STATUS_REASON_CD',
        'ENRL_TERM_BEGIN_DATE',
        'ENRL_TERM_CD',
        'ENRL_TERM_DESCR',
        'ENRL_TERM_END_DATE',
        'ENRL_UNITS_TAKEN',
    ];

    public function klass()
    {
        $klass = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $klass
        );
    }

    public function scopeActiveKlass($query)
    {
        $query->whereNull('CLASS_CANCEL_DATE');
        return $query;
    }

    public function scopeCurrentAndFutureClasses($query)
    {
        $query->where('CLASS_END_DATE', '>=', date('Y-m-d'));
        return $query;
    }
}
