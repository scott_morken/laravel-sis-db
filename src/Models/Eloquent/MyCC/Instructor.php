<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:54 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Instructor extends \Smorken\SisDb\Models\Eloquent\Cds\Instructor implements \Smorken\SisDb\Contracts\Cds\Instructor
{

    protected $table = 'MYCC_PERSON_VW';

    protected $columns = [
        'CLASSM_INSTRUCTOR_EMPLID',
        'CLASSM_INSTRUCTOR_NAME',
        'CLASSM_INSTR_NAME_FLAST',
        'CLASSM_INSTR_EMAIL',
    ];

    protected $relation_map = [
        'instructsKlasses' => KlassInstructor::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'id'         => 'CLASSM_INSTRUCTOR_EMPLID',
                'full_name'  => 'CLASSM_INSTRUCTOR_NAME',
                'short_name' => 'CLASSM_INSTR_NAME_FLAST',
                'email'      => 'CLASSM_INSTR_EMAIL',
            ],
        ];
    }

    public function shortName()
    {
        return $this->arrayGet($this->attributes, 'CLASSM_INSTR_NAME_FLAST');
    }

    public function name()
    {
        return $this->fullName();
    }

    public function fullName()
    {
        return $this->arrayGet($this->attributes, 'CLASSM_INSTRUCTOR_NAME');
    }
}
