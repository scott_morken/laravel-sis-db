<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:49 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Klass extends \Smorken\SisDb\Models\Eloquent\Cds\Klass implements \Smorken\SisDb\Contracts\Cds\Klass
{

    protected $table = 'MYCC_CLASS_VW';

    protected $relation_map = [
        'materials'   => Material::class,
        'enrollments' => Enrollment::class,
        'meetings'    => Meeting::class,
        'instructors' => Instructor::class,
        'term'        => Term::class,
    ];

    public function materials()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns()
                    ->orderBy('TBMAT_DISPLAY_SEQUENCE');
    }

    public function meetings()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns()
                    ->orderBy('CLASSM_CLASS_MTG_NBR');
    }

    public function instructors()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns()
                    ->orderBy('CLASSM_INSTR_ASSIGN_SEQ');
    }
}
