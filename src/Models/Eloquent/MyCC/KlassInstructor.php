<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:51 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

use Smorken\Model\Relations\Traits\FakeRelations;

class KlassInstructor extends \Smorken\SisDb\Models\Eloquent\Cds\KlassInstructor implements \Smorken\SisDb\Contracts\Cds\KlassInstructor
{

    use FakeRelations;

    protected $table = 'MYCC_CLASS_INSTR_VW';

    protected $columns = [
        'CLASS_CLASS_NBR',
        'CLASS_INSTITUTION_CD',
        'CLASS_TERM_CD',
        'CLASSM_ASSIGN_TYPE',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_COURSE_ID',
        'CLASSM_COURSE_OFFER_NBR',
        'CLASSM_INSTR_ASSIGN_SEQ',
        'CLASSM_INSTR_EMAIL',
        'CLASSM_INSTR_LOAD_FACTOR',
        'CLASSM_INSTR_NAME_FLAST',
        'CLASSM_INSTR_TYPE',
        'CLASSM_INSTRUCTOR_EMPLID',
        'CLASSM_INSTRUCTOR_NAME',
        'CLASSM_INSTRUCTOR_ROLE',
        'CLASSM_SECTION',
        'CLASSM_SESSION_CD',
        'CLASSM_TERM_CD',
    ];

    protected $relation_map = [
        'klass'      => Klass::class,
        'meetings'   => Meeting::class,
        'instructor' => Instructor::class,
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['default']['email'] = 'CLASSM_INSTR_EMAIL';
    }

    public function instructor()
    {
        $instr = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $instr
        );
    }

    public function klass()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns();
    }

    public function meetings()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
                    ->defaultColumns();
    }
}
