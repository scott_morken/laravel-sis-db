<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:54 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Material extends \Smorken\SisDb\Models\Eloquent\Rds\Material implements \Smorken\SisDb\Contracts\Rds\Material
{

    protected $table = 'MYCC_CLASS_MATERIALS_VW';

    protected $columns = [
        'CLASS_ACAD_CAREER',
        'CLASS_CLASS_NBR',
        'CLASS_INSTITUTION_CD',
        'CLASS_TERM_CD',
        'TBMAT_AUTHOR',
        'TBMAT_DISPLAY_SEQUENCE',
        'TBMAT_EDITION',
        'TBMAT_ISBN_NBR',
        'TBMAT_NOTES',
        'TBMAT_PRICE',
        'TBMAT_PUBLISHER',
        'TBMAT_SEQ_NBR',
        'TBMAT_STATUS_CD',
        'TBMAT_STATUS_LDESC',
        'TBMAT_TITLE',
        'TBMAT_TYPE_CD',
        'TBMAT_TYPE_CD_LDESC',
        'TBMAT_YEAR_PUBLISHED',
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['sequence'] = 'TBMAT_DISPLAY_SEQUENCE';
        return $c;
    }
}
