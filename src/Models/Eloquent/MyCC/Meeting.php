<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:50 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Meeting extends \Smorken\SisDb\Models\Eloquent\Cds\Meeting implements \Smorken\SisDb\Contracts\Cds\Meeting
{

    protected $table = 'MYCC_CLASS_MTG_VW';

    protected $columns = [
        'CLASS_CLASS_NBR',
        'CLASS_INSTITUTION_CD',
        'CLASS_TERM_CD',
        'CLASSM_BLDG_CD',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_COURSE_ID',
        'CLASSM_COURSE_OFFER_NBR',
        'CLASSM_DAYS_PATTERN',
        'CLASSM_END_DATE',
        'CLASSM_FACILITY_ID',
        'CLASSM_FACILITY_TYPE',
        'CLASSM_LOCATION',
        'CLASSM_MEETING_TIME_END',
        'CLASSM_MEETING_TIME_START',
        'CLASSM_ROOM',
        'CLASSM_SECTION',
        'CLASSM_SESSION_CD',
        'CLASSM_START_DATE',
        'CLASSM_TERM_CD',
    ];

    protected $relation_map = [
        'klass'       => Klass::class,
        'instructors' => KlassInstructor::class,
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['default']['facility_type'] = 'CLASSM_FACILITY_TYPE';
        return $c;
    }

    public function klass()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
                    ->defaultColumns();
    }

    public function instructors()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
                    ->defaultColumns()
                    ->orderBY('CLASSM_INSTR_ASSIGN_SEQ');
    }
}
