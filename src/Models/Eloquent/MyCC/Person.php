<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/26/17
 * Time: 9:40 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Person extends \Smorken\SisDb\Models\Eloquent\Cds\Person implements \Smorken\SisDb\Contracts\Cds\Person
{

    protected $table = 'MYCC_PERSON_VW';

    protected $columns = [
        'PERS_EMPLID',
        'PERS_OPRID',
        'PERS_PRIMARY_FIRST_NAME',
        'PERS_PRIMARY_LAST_NAME',
        'PERS_EMAIL_ADDR',
        'PERS_BIRTHDATE',
        'HRMS_EMPLID',
        'PERS_HASH_DATA1',
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['hash'] = 'PERS_HASH_DATA1';
        return $c;
    }
}
