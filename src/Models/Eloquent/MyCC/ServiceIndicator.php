<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:55 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class ServiceIndicator extends \Smorken\SisDb\Models\Eloquent\Rds\ServiceIndicator implements \Smorken\SisDb\Contracts\Rds\ServiceIndicator
{

    protected $table = 'MYCC_SERV_IND_VW';

    protected $columns = [
        'SRVC_EMPLID',
        'SRVC_IND_DATETIME',
        'SRVC_OPRID',
        'SRVC_INSTITUTION',
        'SRVC_SERVICE_IND_CD',
        'SRVC_SERVICE_IND_LDESC',
        'SRVC_SERVICE_IND_REASON',
        'SRVC_SERVICE_IND_REASON_LDESC',
        'SRVC_SERVICE_IND_ACT_TERM',
        'SRVC_SERVICE_IND_ACTIVE_DT',
        'SRVC_POSITIVE_SRVC_INDICATOR',
        'SRVC_AMOUNT',
        'SRVC_COMM_COMMENTS',
        'SRVC_CONTACT',
        'SRVC_DEPARTMENT_LDESC',
        'SRVC_EXT_DUE_DT',
    ];

    protected $relation_map = [
        'student' => Student::class,
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['default']['due_date'] = function ($attr, $self) {
            return $this->toCarbonDate('SRVC_EXT_DUE_DT');
        };
        return $c;
    }
}
