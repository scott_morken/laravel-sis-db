<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:53 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Student extends \Smorken\SisDb\Models\Eloquent\Cds\Student implements \Smorken\SisDb\Contracts\Cds\Student
{

    protected $table = 'MYCC_PERSON_VW';

    protected $columns = [
        'PERS_EMPLID',
        'PERS_OPRID',
        'PERS_PRIMARY_FIRST_NAME',
        'PERS_PRIMARY_LAST_NAME',
        'PERS_EMAIL_ADDR',
        //'PERS_BIRTHDATE',
        'HRMS_EMPLID',
        'PERS_HASH_DATA1',
    ];

    protected $relation_map = [
        'enrollments'  => Enrollment::class,
        'studentTerms' => StudentTerm::class,
    ];

    public function conversions()
    {
        $c = parent::conversions();
        $c['default']['hash'] = 'PERS_HASH_DATA1';
        return $c;
    }
}
