<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:56 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

use Smorken\Model\Relations\Traits\FakeRelations;

class StudentTerm extends \Smorken\SisDb\Models\Eloquent\Cds\StudentTerm implements \Smorken\SisDb\Contracts\Cds\StudentTerm
{

    use FakeRelations;

    protected $table = 'MYCC_STU_TERM_VW';

    protected $columns = [
        'CTERM_ACAD_CAREER',
        'CTERM_ACAD_LOAD_CODE',
        'CTERM_ACAD_STNDNG_ACTN',
        'CTERM_EMPLID',
        'CTERM_INSTITUTION_CODE',
        'CTERM_INST_ABBREV',
        'CTERM_INST_DESCR50',
        'CTERM_TERM_BEGIN_DATE',
        'CTERM_TERM_CD',
        'CTERM_TERM_DESCR',
        'CTERM_TERM_END_DATE',
        'STFACT_MCCD_CUM_AHRS',
        'STFACT_MCCD_CUM_EHRS',
        'STFACT_MCCD_CUM_QHRS',
        'STFACT_MCCD_CUM_QPTS',
        'STFACT_MCCD_TERM_AHRS',
        'STFACT_MCCD_TERM_EHRS',
        'STFACT_MCCD_TERM_QHRS',
        'STFACT_MCCD_TERM_QPTS',
    ];

    protected $relation_map = [
        'student' => Student::class,
        'term'    => Term::class,
        'college' => College::class,
    ];

    public function college()
    {
        $college = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $college,
            ['id' => 'CTERM_INSTITUTION_CODE', 'description' => 'CTERM_INST_DESCR50', 'abbrev' => 'CTERM_INST_ABBREV']
        );
    }

    public function term()
    {
        $term = $this->getRelationClass(__FUNCTION__);
        return $this->fakeBelongsTo(
            $term,
            [
                'STRM'          => 'CTERM_TERM_CD',
                'DESCR'         => 'CTERM_TERM_DESCR',
                'ACAD_CAREER'   => 'CTERM_ACAD_CAREER',
                'TERM_BEGIN_DT' => 'CTERM_TERM_BEGIN_DATE',
                'TERM_END_DT'   => 'CTERM_TERM_END_DATE',
                'INSTITUTION'   => 'CTERM_INSTITUTION_CODE',
            ]
        );
    }
}
