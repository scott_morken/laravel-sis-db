<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/19/17
 * Time: 11:56 AM
 */

namespace Smorken\SisDb\Models\Eloquent\MyCC;

class Term extends \Smorken\SisDb\Models\Eloquent\Rds\Term implements \Smorken\SisDb\Contracts\Rds\Term
{

    protected $table = 'MYCC_TERM_VW';
}
