<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 12:31 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Athlete extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Athlete
{

    protected $table = 'REC_ATHLETIC_PARTIC_VW';

    protected $columns = [
        'ATHL_INSTITUTION',
        'ATHL_EMPLID',
        'ATHL_SPORT',
        'ATHL_SPORT_LDESC',
        'ATHL_ATHLETIC_PARTIC_CD',
        'ATHL_ATHLETIC_PARTIC_LDESC',
        'ATHL_NCAA_ELIGIBLE',
        'ATHL_CURRENT_PARTICIPANT',
        'ATHL_START_DATE',
        'ATHL_END_DATE',
    ];

    protected $relation_map = [
        'student'     => Student::class,
        'enrollments' => Enrollment::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'college_id'          => 'ATHL_INSTITUTION',
                'student_id'          => 'ATHL_EMPLID',
                'sport_code'          => 'ATHL_SPORT',
                'sport_desc'          => 'ATHL_SPORT_LDESC',
                'status_code'         => 'ATHL_ATHLETIC_PARTIC_CD',
                'status_desc'         => 'ATHL_ATHLETIC_PARTIC_LDESC',
                'ncaa_eligible'       => 'ATHL_NCAA_ELIGIBLE',
                'current_participant' => 'ATHL_CURRENT_PARTICIPANT',
                'start_date'          => function ($attr, $self) {
                    return $this->toCarbonDate('ATHL_START_DATE');
                },
                'end_date'            => function ($attr, $self) {
                    return $this->toCarbonDate('ATHL_END_DATE');
                },
            ],
        ];
    }

    public function name()
    {
        return sprintf('%s-%s-%s', $this->ATHL_INSTITUTION_CD, $this->ATHL_EMPLID, $this->ATHL_SPORT);
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'ATHL_EMPLID', 'EMPLID')->defaultColumns();
    }

    public function enrollments()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'EMPLID', 'ATHL_EMPLID')->defaultColumns();
    }

    public function getATHLSTARTDATEAttribute()
    {
        return $this->toCarbonDate('ATHL_START_DATE');
    }

    public function getATHLENDDATEAttribute()
    {
        return $this->toCarbonDate('ATHL_END_DATE');
    }

    public function scopeActive($query)
    {
        $query->where('ATHL_ATHLETIC_PARTIC_CD', '=', 'ACTIV')
              ->where('ATHL_CURRENT_PARTICIPANT', '=', 'Y');
        return $query;
    }

    public function scopeNcaaEligible($query, $eligible = 'Y')
    {
        $eligible = strtoupper($eligible);
        $query->where('ATHL_NCAA_ELIGIBLE', '=', $eligible);
        return $query;
    }

    public function scopeGreaterThanEqualDate($query, $date)
    {
        $query->whereNull('ATHL_END_DATE')
              ->where('ATHL_START_DATE', '>=', $date);
        return $query;
    }

    public function scopeByCollegeId($query, $college_id)
    {
        return $this->simpleWhere($query, 'ATHL_INSTITUTION', $college_id);
    }

    public function scopeByStudentId($query, $student_id)
    {
        return $this->simpleWhere($query, 'ATHL_EMPLID', $student_id);
    }

    public function scopeJoinStudentAndOrder($query)
    {
        $s = new Student();
        $query->join(
            $s->getTable(),
            sprintf('%s.EMPLID', $s->getTable()),
            '=',
            sprintf('%s.ATHL_EMPLID', $this->getTable())
        )
              ->orderCollege()
              ->orderBy(sprintf('%s.PERS_PRIMARY_LAST_NAME', $s->getTable()))
              ->orderBy(sprintf('%s.PERS_PRIMARY_FIRST_NAME', $s->getTable()));
        return $query;
    }

    public function scopeOrderCollege($query)
    {
        $query->orderBy('ATHL_INSTITUTION');
        return $query;
    }
}
