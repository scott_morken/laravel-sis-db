<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/21/17
 * Time: 10:30 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class CourseCatalog extends BaseRds implements \Smorken\SisDb\Contracts\Rds\CourseCatalog
{

    protected $table = 'REC_CRSE_CATALOG_VW';

    protected $columns = [
        'CRSE_REC_KEY',
        'CRSE_CATALOG_SID',
        'CRSE_CRSE_ID',
        'CRSE_DESCR',
        'CRSE_CONSENT',
        'CRSE_UNITS_MINIMUM', //credits
        'CRSE_UNITS_MAXIMUM',
        'CRSE_GRADING_BASIS',
        'CRSE_COMPONENT',
        'CRSE_COURSE_NOTES',
        'CRSE_GOV_BD_APPROVAL_DATE',
        'CRSE_EFFDT',
    ];

    protected $relation_map = [
        'offerings' => CourseOffer::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'id'             => 'CRSE_REC_KEY',
                'catalog_id'     => 'CRSE_CATALOG_SID',
                'course_id'      => 'CRSE_CRSE_ID',
                'description'    => 'CRSE_DESCR',
                'consent'        => 'CRSE_CONSENT',
                'units'          => 'CRSE_UNITS_MINIMUM', //credits
                'units_max'      => 'CRSE_UNITS_MAXIMUM',
                'grading_basis'  => 'CRSE_GRADING_BASIS',
                'component_code' => 'CRSE_COMPONENT',
                'notes'          => 'CRSE_COURSE_NOTES',
                'approval_date'  => 'CRSE_GOV_BD_APPROVAL_DATE',
                'effective_date' => 'CRSE_EFFDT',
            ],
        ];
    }

    public function name()
    {
        return sprintf('%s-%s-%s', $this->CRSE_REC_KEY, $this->CRSE_DESCR);
    }

    public function offerings()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            ['CRSE_CATALOG_SID', 'COFFR_CRSE_ID'],
            ['CRSE_CATALOG_SID', 'CRSE_CRSE_ID']
        )->defaultColumns();
    }

    public function scopeByCollegeId($q, $college_id)
    {
        return $q->whereHas(
            'courses',
            function ($qs) use ($college_id) {
                return $qs->where('COFFR_INSTITUTION', '=', $college_id);
            }
        );
    }
}
