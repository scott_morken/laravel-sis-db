<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/21/17
 * Time: 10:45 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

use Smorken\SisDb\Models\Eloquent\Cds\CourseNote;
use Smorken\SisDb\Models\Eloquent\Traits\Converters;

class CourseOffer extends BaseRds implements \Smorken\SisDb\Contracts\Rds\CourseOffer
{

    use Converters;

    protected $table = 'REC_CRSE_OFFER_VW';

    protected $columns = [
        'COFFR_ACAD_CAREER',
        'COFFR_ACAD_GROUP',
        'COFFR_ACAD_ORG',
        'COFFR_CAMPUS',
        'COFFR_CATALOG_NBR',
        'COFFR_CATALOG_PRINT',
        'COFFR_CLASS_SID',
        'COFFR_COURSE_APPROVED',
        'COFFR_CRSE_EFFDT',
        'COFFR_CRSE_ID',
        'COFFR_CRSE_OFFER_NBR',
        'COFFR_INSTITUTION',
        'COFFR_REC_KEY',
        'COFFR_SCHEDULE_PRINT',
        'COFFR_SUBJECT',
        'CRSE_CATALOG_SID',
    ];

    protected $relation_map = [
        'courseCatalog' => CourseCatalog::class,
        'courseNotes'   => CourseNote::class,

    ];

    public function conversions()
    {
        return [
            'default' => [
                'acad_career'    => 'COFFR_ACAD_CAREER',
                'acad_group'     => 'COFFR_ACAD_GROUP',
                'acad_org'       => 'COFFR_ACAD_ORG',
                'approved'       => 'COFFR_COURSE_APPROVED',
                'campus'         => 'COFFR_CAMPUS',
                'catalog_id'     => 'CRSE_CATALOG_SID',
                'catalog_number' => 'COFFR_CATALOG_NBR',
                'class_id'       => 'COFFR_CLASS_SID',
                'college_id'     => 'COFFR_INSTITUTION',
                'course_id'      => 'COFFR_CRSE_ID',
                'effective_date' => 'COFFR_CRSE_EFFDT',
                'id'             => 'COFFR_REC_KEY',
                'offer_number'   => 'COFFR_CRSE_OFFER_NBR',
                'subject'        => 'COFFR_SUBJECT',
                'catalog_print'  => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'COFFR_CATALOG_PRINT') === 'Y');
                },
                'description'    => function ($attr, $self) {
                    return ($this->courseCatalog ? $this->courseCatalog->description : 'N/A');
                },
                'schedule_print' => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'COFFR_SCHEDULE_PRINT') === 'Y');
                },
                'consent'        => function ($attr, $self) {
                    return $this->courseCatalog ? $this->courseCatalog->consent : '';
                },
                'component_code' => function ($attr, $self) {
                    return $this->courseCatalog ? $this->courseCatalog->component_code : '';
                },
                'units'          => function ($attr, $self) {
                    return $this->courseCatalog ? $this->courseCatalog->units : '';
                },
                'grading_basis'  => function ($attr, $self) {
                    return $this->courseCatalog ? $this->courseCatalog->grading_basis : '';
                },
            ],
        ];
    }

    public function name()
    {
        return sprintf(
            ' %s %s %s',
            $this->arrayGet($this->attributes, 'COFFR_REC_KEY'),
            $this->arrayGet($this->attributes, 'COFFR_SUBJECT'),
            $this->arrayGet($this->attributes, 'COFFR_CATALOG_NBR')
        );
    }

    public function getCOFFRCATALOGNBRAttribute($k)
    {
        return trim($this->arrayGet($this->attributes, 'COFFR_CATALOG_NBR'));
    }

    public function courseNotes()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        $columns_from = [
            'COFFR_CRSE_ID',
            'COFFR_CRSE_OFFER_NBR',
        ];
        $columns_to = [
            'CLASS_COURSE_ID',
            'CLASS_COURSE_OFFER_NBR',
        ];
        if ($this->term_id) {
            $columns_from[] = 'term_id';
            $columns_to[] = 'CLASS_TERM_CD';
        }
        return $this->compositeHasMany(
            $rel,
            $columns_to,
            $columns_from
        )
                    ->defaultColumns();
    }

    public function scopeJoinCourseCatalog($query)
    {
        $cc = new CourseCatalog();
        $query->select(sprintf('%s.*', $this->getTable()))
              ->addSelect(
                  [
                      'CRSE_REC_KEY',
                      'CRSE_CRSE_ID',
                      'CRSE_DESCR',
                      'CRSE_CONSENT',
                      'CRSE_UNITS_MINIMUM',
                      'CRSE_UNITS_MAXIMUM',
                      'CRSE_GRADING_BASIS',
                      'CRSE_COMPONENT',
                      'CRSE_COURSE_NOTES',
                      'CRSE_GOV_BD_APPROVAL_DATE',
                      'CRSE_EFFDT',
                  ]
              );
        return $query->complexJoin(
            $cc->getTable(),
            $this->getTable(),
            [
                'CRSE_CATALOG_SID' => 'CRSE_CATALOG_SID',
                'CRSE_CRSE_ID'     => 'COFFR_CRSE_ID',
            ]
        );
    }

    public function courseCatalog()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeBelongsTo(
            $rel,
            ['CRSE_CATALOG_SID', 'COFFR_CRSE_ID'],
            ['CRSE_CATALOG_SID', 'CRSE_CRSE_ID']
        )->defaultColumns();
    }

    public function courseCatalogJoined()
    {
        $rel = $this->getRelationClass('courseCatalog');
        return $this->belongsToModel(
            $rel,
            [
                'CRSE_REC_KEY',
                'CRSE_CATALOG_SID',
                'CRSE_CRSE_ID',
                'CRSE_DESCR',
                'CRSE_CONSENT',
                'CRSE_UNITS_MINIMUM', //credits
                'CRSE_UNITS_MAXIMUM',
                'CRSE_GRADING_BASIS',
                'CRSE_COMPONENT',
                'CRSE_COURSE_NOTES',
                'CRSE_GOV_BD_APPROVAL_DATE',
                'CRSE_EFFDT',
            ],
            'courseCatalog'
        );
    }

    public function scopeCollegeIdIs($q, $college_id)
    {
        return $q->where('COFFR_INSTITUTION', '=', $college_id);
    }

    public function scopeSubjectAndCatalogNbrIs($q, $subject, $catalog_nbr)
    {
        $catalog_nbr = strlen($catalog_nbr) === 4 ? $catalog_nbr : '_' . $catalog_nbr;
        return $q->where('COFFR_SUBJECT', '=', $subject)
                 ->where('COFFR_CATALOG_NBR', 'LIKE', $catalog_nbr);
    }

    public function scopeApproved($q)
    {
        return $q->where('COFFR_COURSE_APPROVED', '=', 'A');
    }

    public function scopeOrderSubjectCatalogNbr($q)
    {
        return $q->orderBy('COFFR_SUBJECT')
                 ->orderBy('COFFR_CATALOG_NBR');
    }

    public function scopeAcademicOrgIdIs($q, $acad_org_id)
    {
        return $q->where('COFFR_ACAD_ORG', '=', $acad_org_id);
    }
}
