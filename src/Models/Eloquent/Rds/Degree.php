<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/2/16
 * Time: 2:28 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Degree extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Degree
{

    protected $table = 'PCC_REC_DEGREE_PLAN_VW';

    protected $columns = [
        'INSTITUTION',
        'ACAD_PROG',
        'ACAD_PLAN_TYPE',
        'ACAD_PLAN',
        'EFFDT',
        'EFF_STATUS',
        'FIRST_TERM_VALID',
        'DESCR',
        'DEGREE',
        'DEGREE_DESCR',
        'DIPLOMA_DESCR',
        'TRNSCR_DESCR',
        'MIN_UNITS_REQD',
        'PLAN_DESCRIPTION',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'college_id'       => 'INSTITUTION',
                'acad_prog_code'   => 'ACAD_PROG',
                'plan_type_code'   => 'ACAD_PLAN_TYPE',
                'plan_code'        => 'ACAD_PLAN',
                'effective_date'   => function ($attr, $self) {
                    return $this->toCarbonDate('EFFDT');
                },
                'status'           => 'EFF_STATUS',
                'term_active'      => 'FIRST_TERM_VALID',
                'short_desc'       => 'DESCR',
                'degree_code'      => 'DEGREE',
                'degree_desc'      => 'DEGREE_DESCR',
                'diploma_desc'     => 'DIPLOMA_DESCR',
                'transcript_desc'  => 'TRNSCR_DESCR',
                'credits_required' => 'MIN_UNITS_REQD',
                'long_desc'        => 'PLAN_DESCRIPTION',
            ],
        ];
    }

    public function name()
    {
        return sprintf('%s', $this->transcript_desc);
    }

    public function getEFFDTAttribute($k)
    {
        return $this->toCarbonDate('EFFDT');
    }

    public function scopeActive($query)
    {
        $query->where('EFF_STATUS', '=', 'A')
              ->whereNotNull('DEGREE_EFFDT')
              ->where('DESCRSHORT', '<>', 'CONV');
        return $query;
    }

    public function scopeCredit($query)
    {
        $query->where('ACAD_PROG', '<>', 'NC');
        return $query;
    }

    public function scopePlanCodeIs($query, $plan_code)
    {
        $query->where('ACAD_PLAN', '=', $plan_code);
        return $query;
    }

    public function scopeOrderDesc($query)
    {
        $query->orderBy('INSTITUTION')
              ->orderBy('TRNSCR_DESCR')
              ->orderBy('DEGREE');
        return $query;
    }
}
