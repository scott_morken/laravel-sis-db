<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 12:31 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Enrollment extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Enrollment
{

    protected $table = 'RDS_STU_CLASS_VW';

    protected $columns = [
        'CLASS_SID',
        'EMPLID',
        'ENRL_INSTITUTION_CD',
        'ENRL_TERM_CD',
        'ENRL_CLASS_NBR',
        'ENRL_ACAD_CAREER',
        'ENRL_GRADING_BASIS',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_UNITS_TAKEN',
        'ENRL_STATUS_REASON_CD',
        'ENRL_STATUS_REASON_LDESC',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_INCLUDE_IN_GPA',
    ];

    protected $relation_map = [
        'klass'     => Klass::class,
        'student'   => Student::class,
        'materials' => Material::class,
        'term'      => Term::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'       => 'EMPLID',
                'college_id'       => 'ENRL_INSTITUTION_CD',
                'term_id'          => 'ENRL_TERM_CD',
                'class_number'     => 'ENRL_CLASS_NBR',
                'acad_career'      => 'ENRL_ACAD_CAREER',
                'grading_basis'    => 'ENRL_GRADING_BASIS',
                'grade'            => 'ENRL_OFFICIAL_GRADE',
                'grade_date'       => function ($attr, $self) {
                    return $this->toCarbonDate('ENRL_GRADE_DATE');
                },
                'units_taken'      => 'ENRL_UNITS_TAKEN',
                'grade_points'     => 'ENRL_GRADE_POINTS',
                'status_code'      => 'ENRL_STATUS_REASON_CD',
                'status_desc'      => 'ENRL_STATUS_REASON_LDESC',
                'last_action_code' => 'ENRL_ACTN_RSN_LAST_CD',
                'last_action_desc' => 'ENRL_ACTN_RSN_LAST_LDESC',
                'include_in_gpa'   => 'ENRL_INCLUDE_IN_GPA',
            ],
        ];
    }

    public function name()
    {
        return sprintf(
            '%s-%s-%s',
            $this->arrayGet($this->attributes, 'CLASS_SID'),
            $this->arrayGet($this->attributes, 'ENRL_INSTITUTION_CD'),
            $this->arrayGet($this->attributes, 'EMPLID')
        );
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'EMPLID', 'EMPLID')->defaultColumns();
    }

    public function klass()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'CLASS_SID', 'CLASS_SID')->defaultColumns();
    }

    public function materials()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'TBMAT_SID', 'CLASS_SID')
                    ->defaultColumns()
                    ->orderSequence();
    }

    public function term()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'ENRL_TERM_CD', 'STRM')->defaultColumns();
    }

    public function getENRLGRADEDATEAttribute()
    {
        return $this->toCarbonDate('ENRL_GRADE_DATE');
    }

    public function scopeByTermId($query, $term_id)
    {
        return $this->whereTermLike('ENRL_TERM_CD', $query, $term_id);
    }

    public function scopeByCollegeId($query, $college_id)
    {
        return $this->simpleWhere($query, 'ENRL_INSTITUTION_CD', $college_id);
    }

    public function scopeByStudentId($query, $student_id)
    {
        return $this->simpleWhere($query, 'EMPLID', $student_id);
    }

    public function scopeEnrolled($query)
    {
        $query->whereIn('ENRL_STATUS_REASON_CD', ['ENRL', 'EWAT']);
        return $query;
    }

    public function scopeGraded($query)
    {
        $query->whereNotNull('ENRL_GRADE_DATE');
        return $query;
    }

    public function scopeIncludeInGpa($query)
    {
        $this->simpleWhere($query, 'ENRL_INCLUDE_IN_GPA', 'Y');
    }

    public function scopeCredit($query)
    {
        $query->where('ENRL_ACAD_CAREER', '=', 'CRED');
        return $query;
    }

    public function scopeByCourseId($query, $course_id)
    {
        $query->whereHas(
            'klass',
            function ($q) use ($course_id) {
                $q->where('CLASS_CLASS_NAME', '=', $course_id);
            }
        );
        return $query;
    }

    public function scopeActiveKlass($query)
    {
        $query->whereHas(
            'klass',
            function ($q) {
                $q->whereNull('CLASS_CANCEL_DATE');
            }
        );
        return $query;
    }

    public function scopeActiveAndUpcomingTerm($query)
    {
        return $this->scopeTermEndingAfter($query, date('Y-m-d'));
    }

    public function scopeTermEndingAfter($query, $date = '-3 years')
    {
        $date = strtotime($date);
        $query->whereHas(
            'term',
            function ($q) use ($date) {
                $q->where('TERM_END_DT', '>=', $date);
            }
        );
        return $query;
    }

    public function scopeOrderReverseTerm($query)
    {
        $query->orderBy('ENRL_INSTITUTION_CD')
              ->orderBy('ENRL_TERM_CD', 'desc')
              ->orderBy('ENRL_STATUS_REASON_CD', 'desc');
        return $query;
    }

    public function scopeOrderStudent($query)
    {
        $query->orderBy('ENRL_INSTITUTION_CD')
              ->orderBy('ENRL_TERM_CD', 'desc')
              ->orderBy('EMPLID');
        return $query;
    }
}
