<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/9/17
 * Time: 12:17 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Facility extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Facility
{

    protected $table = 'REC_FACILITY_VW';

    protected $columns = [
        'FACILITY_SETID',
        'FACILITY_ID',
        'FACILITY_EFF_STATUS',
        'FACILITY_BLDG_CD',
        'FACILITY_ROOM',
        'FACILITY_DESCR',
        'FACILITY_DESCRSHORT',
        'FACILITY_GROUP',
        'FACILITY_LOCATION',
        'FACILITY_CAPACITY',
        'FACILITY_ACAD_ORG',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'college_id'    => 'FACILITY_SETID',
                'id'            => 'FACILITY_ID',
                'status'        => 'FACILITY_EFF_STATUS',
                'building_code' => 'FACILITY_BLDG_CD',
                'room'          => 'FACILITY_ROOM',
                'description'   => 'FACILITY_DESCR',
                'short_descr'   => 'FACILITY_DESCRSHORT',
                'group'         => 'FACILITY_GROUP',
                'location_code' => 'FACILITY_LOCATION',
                'capacity'      => 'FACILITY_CAPACITY',
                'acad_org'      => 'FACILITY_ACAD_ORG',
            ],
        ];
    }

    public function scopeOrderDescription($q)
    {
        return $q->orderBy('FACILITY_DESCR');
    }

    public function scopeCollegeIdIs($q, $college_id)
    {
        return $q->where('FACILITY_SETID', '=', $college_id);
    }

    public function scopeActive($q)
    {
        return $q->where('FACILITY_EFF_STATUS', '=', 'A');
    }

    public function scopeIdIs($q, $id)
    {
        return $q->where('FACILITY_ID', '=', $id);
    }

    public function scopeDescriptionLike($q, $search)
    {
        $search = '%' . $search . '%';
        return $q->where('FACILITY_DESCR', 'LIKE', $search);
    }

    public function scopeAcademicOrgIs($q, $org)
    {
        return $q->where('FACILITY_ACAD_ORG', '=', $org);
    }

    public function scopeLocationCodeIs($q, $location_code)
    {
        return $q->where('FACILITY_LOCATION', '=', $location_code);
    }
}
