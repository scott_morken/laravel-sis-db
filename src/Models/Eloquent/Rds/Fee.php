<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/2/17
 * Time: 12:05 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Fee extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Fee
{

    protected $table = 'RDS_CLASS_SUBFEE_VW';

    protected $columns = [
        'CLSF_SID',
        'CLASS_SID',
        'CLSBF_SETID', //college id
        'CLSBF_CRSE_ID',
        'CLSBF_CRSE_OFFER_NBR',
        'CLSBF_STRM',
        'CLSBF_SESSION_CODE',
        'CLSBF_CLASS_SECTION',
        'CLSBF_COMPONENT',
        'CLSBF_ACCOUNT_TYPE_SF',
        'CLSBF_ITEM_TYPE',
        'CLSBF_ITEM_TYPE_SDESC',
        'CLSBF_ITEM_TYPE_LDESC',
        'CLSBF_FLAT_AMT',
        'CLSBF_CURRENCY_CD',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'college_id'        => 'CLSBF_SETID', //college id
                'term_id'           => 'CLSBF_STRM',
                'account_type'      => 'CLSBF_ACCOUNT_TYPE_SF',
                'item_type'         => 'CLSBF_ITEM_TYPE',
                'short_description' => 'CLSBF_ITEM_TYPE_SDESC',
                'description'       => 'CLSBF_ITEM_TYPE_LDESC',
                'amount'            => 'CLSBF_FLAT_AMT',
                'currency'          => 'CLSBF_CURRENCY_CD',
            ],
        ];
    }

    public function getCLSBFFLATAMTAttribute()
    {
        return number_format((float)$this->arrayGet($this->attributes, 'CLSBF_FLAT_AMT', 0), 2);
    }

    public function scopeCollegeIdIs($q, $college_id)
    {
        return $q->where('CLSBF_SETID', '=', $college_id);
    }

    public function scopeDescriptionLike($q, $search)
    {
        $search = '%' . $search . '%';
        return $q->where('CLSBF_ITEM_TYPE_LDESC', 'LIKE', $search);
    }
}
