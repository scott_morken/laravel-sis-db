<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/19/16
 * Time: 9:32 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Group extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Group
{

    protected $table = 'RDS_STU_GROUPS_VW';

    protected $columns = [
        'STDGR_EMPLID',
        'STDGR_INSTITUTION',
        'STDGR_STUDENT_GROUP',
        'STDGR_STDNT_GROUP_LDESC',
        'STDGR_START_DATE',
        'STDGR_END_DATE',
        'STDGR_STUDENT_STATUS',
        'STDGR_STUDENT_STATUS_LDESC',
        'STDGR_COMMENTS',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'  => 'STDGR_EMPLID',
                'college_id'  => 'STDGR_INSTITUTION',
                'id'          => 'STDGR_STUDENT_GROUP',
                'description' => 'STDGR_STDNT_GROUP_LDESC',
                'start_date'  => function ($attr, $self) {
                    return $this->toCarbonDate('STDGR_START_DATE');
                },
                'end_date'    => function ($attr, $self) {
                    return $this->toCarbonDate('STDGR_END_DATE');
                },
                'status_code' => 'STDGR_STUDENT_STATUS',
                'status_desc' => 'STDGR_STUDENT_STATUS_LDESC',
                'comments'    => 'STDGR_COMMENTS',
            ],
        ];
    }

    public function getSTDGRSTARTDATEAttribute($v)
    {
        return $this->toCarbonDate('STDGR_START_DATE');
    }

    public function getSTDGRENDDATEAttribute($v)
    {
        return $this->toCarbonDate('STDGR_END_DATE');
    }

    public function scopeActive($query)
    {
        $query->where('STDGR_STUDENT_STATUS', '=', 'A');
        return $query;
    }
}
