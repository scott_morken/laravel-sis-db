<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 12:31 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

use Smorken\SisDb\Models\Eloquent\Cds\CourseNote;
use Smorken\SisDb\Models\Eloquent\Cds\KlassNote;
use Smorken\SisDb\Models\Eloquent\Traits\Converters;

class Klass extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Klass
{

    use \Smorken\SisDb\Models\Eloquent\Traits\Klass, Converters;

    protected $table = 'RDS_CLASS_VW';

    protected $columns = [
        'CASSC_GRADING_BASIS',
        'CASSC_HONORS_YN',
        'CASSC_UNITS_MINIMUM',
        'CLASS_ACAD_CAREER',
        'CLASS_ACAD_ORG',
        'CLASS_ACAD_ORG_LDESC',
        'CLASS_CATALOG_NBR',
        'CLASS_CLASS_NAME',
        'CLASS_CLASS_NBR',
        'CLASS_CLASS_STAT',
        'CLASS_CLASS_STAT_LDESC',
        'CLASS_COMBINED_ID',
        'CLASS_COMBINED_DESCR',
        'CLASS_COMBINED_MAJOR_YN',
        'CLASS_COMBINED_SECTION',
        'CLASS_COMPONENT_CD',
        'CLASS_COMPONENT_LDESC',
        'CLASS_CONSENT',
        'CLASS_CONSENT_LDESC',
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_DAY_EVE',
        'CLASS_DAY_EVE_LDESC',
        'CLASS_DEPTID_EXPENSE_CD',
        'CLASS_DESCR',
        'CLASS_END_DATE',
        'CLASS_ENRL_CAP',
        'CLASS_ENRL_STAT',
        'CLASS_ENRL_STAT_LDESC',
        'CLASS_ENRL_TOT',
        'CLASS_HS_DUAL_ENROLL_YN',
        'CLASS_HS_DUAL_SPONSOR',
        'CLASS_INSTITUTION_CD',
        'CLASS_INSTRUCTION_MODE',
        'CLASS_INSTRUCTION_MODE_LDESC',
        'CLASS_INSTRUCTOR_EMPLID',
        'CLASS_INSTRUCTOR_NAME',
        'CLASS_LOCATION_CD',
        'CLASS_LOCATION_LDESC',
        'CLASS_MAJOR_CLASS_NAME',
        'CLASS_MAJOR_CLASS_NBR',
        'CLASS_SCHEDULE_PRINT',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_SID',
        'CLASS_START_DATE',
        'CLASS_SUBJECT_CD',
        'CLASS_TERM_CD',
        'COFFR_CLASS_SID',
    ];

    protected $relation_map = [
        'materials'   => \Smorken\SisDb\Models\Eloquent\Rds\Material::class,
        'enrollments' => \Smorken\SisDb\Models\Eloquent\Cds\Enrollment::class,
        'meetings'    => \Smorken\SisDb\Models\Eloquent\Cds\Meeting::class,
        'instructors' => \Smorken\SisDb\Models\Eloquent\Cds\KlassInstructor::class,
        'term'        => \Smorken\SisDb\Models\Eloquent\Rds\Term::class,
        'klassNotes'  => KlassNote::class,
        'courseNotes' => CourseNote::class,
        'fees'        => Fee::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'acad_career'             => 'CLASS_ACAD_CAREER',
                'acad_org'                => 'CLASS_ACAD_ORG',
                'acad_org_desc'           => 'CLASS_ACAD_ORG_LDESC',
                'catalog_number'          => 'CLASS_CATALOG_NBR',
                'class_name'              => 'CLASS_CLASS_NAME',
                'class_number'            => 'CLASS_CLASS_NBR',
                'college_id'              => 'CLASS_INSTITUTION_CD',
                'combined_id'             => 'CLASS_COMBINED_ID',
                'component_code'          => 'CLASS_COMPONENT_CD',
                'component_desc'          => 'CLASS_COMPONENT_LDESC',
                'consent'                 => 'CLASS_CONSENT',
                'consent_desc'            => 'CLASS_CONSENT_LDESC',
                'course_offer_number'     => 'CLASS_COURSE_OFFER_NBR',
                'day_evening_code'        => 'CLASS_DAY_EVE',
                'day_evening_desc'        => 'CLASS_DAY_EVE_LDESC',
                'department_id'           => 'CLASS_DEPTID_EXPENSE_CD',
                'description'             => 'CLASS_DESCR',
                'dual_enroll_sponsor'     => 'CLASS_HS_DUAL_SPONSOR',
                'end_date'                => function ($attr, $self) {
                    return $this->toCarbonDate('CLASS_END_DATE');
                },
                'enrolled'                => 'CLASS_ENRL_TOT',
                'enrolled_cap'            => 'CLASS_ENRL_CAP',
                'enrollment_status'       => 'CLASS_ENRL_STAT',
                'enrollment_status_desc'  => 'CLASS_ENRL_STAT_LDESC',
                'grading_basis'           => 'CASSC_GRADING_BASIS',
                'instruction_mode'        => 'CLASS_INSTRUCTION_MODE',
                'instruction_mode_desc'   => 'CLASS_INSTRUCTION_MODE_LDESC',
                'location_code'           => 'CLASS_LOCATION_CD',
                'location_desc'           => 'CLASS_LOCATION_LDESC',
                'major_class_name'        => 'CLASS_MAJOR_CLASS_NAME',
                'major_class_number'      => 'CLASS_MAJOR_CLASS_NBR',
                'primary_instructor_id'   => 'CLASS_INSTRUCTOR_EMPLID',
                'primary_instructor_name' => 'CLASS_INSTRUCTOR_NAME',
                'section'                 => 'CLASS_SECTION',
                'session_code'            => 'CLASS_SESSION_CD',
                'session_type'            => 'CLASS_SESSION_CD',
                'start_date'              => function ($attr, $self) {
                    return $this->toCarbonDate('CLASS_START_DATE');
                },
                'status'                  => 'CLASS_CLASS_STAT',
                'status_desc'             => 'CLASS_CLASS_STAT_LDESC',
                'subject'                 => 'CLASS_SUBJECT_CD',
                'term_id'                 => 'CLASS_TERM_CD',
                'units'                   => 'CASSC_UNITS_MINIMUM',
                'combined_major'          => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CLASS_COMBINED_MAJOR_YN') === 'Y');
                },
                'major_class'             => function ($attr, $self) {
            $name = $this->arrayGet($this->attributes, 'CLASS_MAJOR_CLASS_NAME');
                    if ($name) {
                        $nbr = $this->arrayGet($this->attributes, 'CLASS_MAJOR_CLASS_NBR');
                        return sprintf('%s %s', $name, $nbr);
                    }
                },
                'combined_section'        => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CLASS_COMBINED_SECTION') === 'C');
                },
                'combined_list'           => function ($attr, $self) {
                    if ($this->combined_section) {
                        return str_replace('-', ' ', $this->arrayGet($this->attributes, 'CLASS_COMBINED_DESCR'));
                    }
                },
                'dual_enroll'             => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CLASS_HS_DUAL_ENROLL_YN') === 'Y');
                },
                'honors'                  => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CASSC_HONORS_YN') === 'Y');
                },
                'rollover'                => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CLASS_CLASS_ROLL_YN') === 'Y');
                },
                'schedule_print'          => function ($attr, $self) {
                    return $this->boolToInt($this->arrayGet($this->attributes, 'CLASS_SCHEDULE_PRINT') === 'Y');
                },
            ],
        ];
    }

    public function name()
    {
        return sprintf('%s', $this->arrayGet($this->attributes, 'CLASS_NAME'));
    }

    public function enrollments()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'CLASS_SID', 'CLASS_SID')
                    ->defaultColumns();
    }

    public function materials()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'CLASS_SID', 'TBMAT_SID')
                    ->defaultColumns();
    }

    public function fees()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany($rel, 'CLASS_SID', 'CLASS_SID')
                    ->defaultColumns();
    }

    public function scopeDepartmentIdIn($query, $dept_ids)
    {
        if (is_array($dept_ids) && isset($dept_ids[0]) && $dept_ids[0] === '*') {
            return $query;
        }
        $query->whereIn('CLASS_DEPTID_EXPENSE_CD', $dept_ids);
        return $query;
    }
}
