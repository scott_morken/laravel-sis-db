<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 12:31 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Material extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Material
{

    protected $table = 'REC_CLASS_MATERIALS_VW';

    protected $columns = [
        'TBMAT_SID',
        'TBMAT_SEQ_NBR',
        'TBMAT_TYPE_CD',
        'TBMAT_TYPE_CD_LDESC',
        'TBMAT_STATUS_CD',
        'TBMAT_STATUS_LDESC',
        'TBMAT_TITLE',
        'TBMAT_ISBN_NBR',
        'TBMAT_AUTHOR',
        'TBMAT_PUBLISHER',
        'TBMAT_EDITION',
        'TBMAT_YEAR_PUBLISHED',
        'TBMAT_PRICE',
        'TBMAT_NOTES',
    ];

    public function conversions()
    {
        return [
            'default' => [
                'sequence_number' => 'TBMAT_SEQ_NBR',
                'type_code'       => 'TBMAT_TYPE_CD',
                'type_desc'       => 'TBMAT_TYPE_CD_LDESC',
                'status_code'     => 'TBMAT_STATUS_CD',
                'status_desc'     => 'TBMAT_STATUS_LDESC',
                'title'           => 'TBMAT_TITLE',
                'isbn'            => 'TBMAT_ISBN_NBR',
                'author'          => 'TBMAT_AUTHOR',
                'publisher'       => 'TBMAT_PUBLISHER',
                'edition'         => 'TBMAT_EDITION',
                'year_published'  => 'TBMAT_YEAR_PUBLISHED',
                'price'           => 'TBMAT_PRICE',
                'notes'           => 'TBMAT_NOTES',
            ],
        ];
    }

    public function name()
    {
        return sprintf('%s', $this->title);
    }

    public function scopeOrderSequence($query)
    {
        $query->orderBy('TBMAT_SEQ_NBR');
        return $query;
    }
}
