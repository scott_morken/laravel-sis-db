<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 1:37 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Person extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Person
{

    protected $table = 'RDS_PERSON_VW';

    protected $columns = [
        'EMPLID',
        'PERS_OPRID',
        'PERS_PREFERRED_FIRST_NAME',
        'PERS_PREFERRED_LAST_NAME',
        'PERS_PRIMARY_FIRST_NAME',
        'PERS_PRIMARY_LAST_NAME',
        'EMAIL_EMAIL_ADDR',
        'PERS_BIRTHDATE',
        'HRMS_EMPLID',
    ];

    public function name()
    {
        return $this->fullName();
    }

    public function fullName()
    {
        return sprintf('%s %s', $this->getFirstName(null), $this->getLastName(null));
    }

    public function getFirstName($v)
    {
        return trim($this->arrayGet($this->attributes, 'PERS_PREFERRED_FIRST_NAME'))
            ?: $this->arrayGet(
                $this->attributes,
                'PERS_PRIMARY_FIRST_NAME'
            );
    }

    public function getLastName($v)
    {
        return trim($this->arrayGet($this->attributes, 'PERS_PREFERRED_LAST_NAME'))
            ?: $this->arrayGet(
                $this->attributes,
                'PERS_PRIMARY_LAST_NAME'
            );
    }

    public function shortName()
    {
        return sprintf('%s. %s', substr($this->getFirstName(null), 0, 1), $this->getLastName(null));
    }

    public function conversions()
    {
        return [
            'default' => [
                'id'         => 'EMPLID',
                'alt_id'     => 'PERS_OPRID',
                'first_name' => function ($attr, $self) {
                    return $self->getFirstName($attr);
                },
                'last_name'  => function ($attr, $self) {
                    return $self->getLastName($attr);
                },
                'email'      => 'EMAIL_EMAIL_ADDR',
                'dob'        => 'PERS_BIRTHDATE',
                'hrms_id'    => 'HRMS_EMPLID',
            ],
        ];
    }
}
