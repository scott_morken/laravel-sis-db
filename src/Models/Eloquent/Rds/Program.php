<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 1:25 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Program extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Program
{

    protected $table = 'RDS_PROG_PLAN_VW';

    protected $columns = [
        'STRUC_EMPLID',
        'STRUC_INSTITUTION',
        'STRUC_ACAD_PROG_LDESC',
        'STRUC_ACAD_PROG_STATUS',
        'STRUC_ACAD_PROG_STATUS_LDESC',
        'STRUC_PROG_REASON_LDESC',
        'STRUC_PROG_ADMIT_TERM',
        'STRUC_ACAD_PLAN_LDESC',
        'STRUC_PLAN_TYPE_LDESC',
        'STRUC_ACAD_PLAN_DEGREE',
        'STRUC_EXP_GRAD_TERM',
        'STRUC_ACAD_INTEREST_LDESC',
        'STRUC_DEGR_CHECKOUT_STAT',
        'STRUC_DEGR_CHECKOUT_LDESC',
    ];

    protected $relation_map = [
        'student' => Student::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'          => 'STRUC_EMPLID',
                'college_id'          => 'STRUC_INSTITUTION',
                'program_type'        => 'STRUC_ACAD_PROG_LDESC',
                'program_status_code' => 'STRUC_ACAD_PROG_STATUS',
                'program_status'      => 'STRUC_ACAD_PROG_STATUS_LDESC',
                'program_last_action' => 'STRUC_PROG_REASON_LDESC',
                'admit_term'          => 'STRUC_PROG_ADMIT_TERM',
                'plan_name'           => 'STRUC_ACAD_PLAN_LDESC',
                'plan_type'           => 'STRUC_PLAN_TYPE_LDESC',
                'award_type'          => 'STRUC_ACAD_PLAN_DEGREE',
                'grad_term'           => 'STRUC_EXP_GRAD_TERM',
                'grad_status_code'    => 'STRUC_DEGR_CHECKOUT_STAT',
                'grad_status_desc'    => 'STRUC_DEGR_CHECKOUT_LDESC',
                'interest'            => 'STRUC_ACAD_INTEREST_LDESC',
            ],
        ];
    }

    public function name()
    {
        return sprintf(
            "%s %s %s",
            $this->arrayGet($this->attributes, 'STRUC_EMPLID'),
            $this->arrayGet($this->attributes, 'STRUC_INSTITUTION'),
            $this->arrayGet($this->attributes, 'STRUC_ACAD_PLAN_LDESC')
        );
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'STRUC_EMPLID', 'EMPLID')->defaultColumns();
    }

    public function scopeDegreeAwarded($query)
    {
        return $this->simpleWhere($query, 'STRUC_DEGR_CHECKOUT_STAT', 'AW');
    }

    public function scopeNoSubPlan($query)
    {
        $query->whereNull('STRUC_ACAD_SUB_PLAN');
        return $query;
    }

    public function scopeActiveDegree($query)
    {
        $query->active()->degree();
        return $query;
    }

    public function scopeActive($query)
    {
        $query->where('STRUC_ACAD_PROG_STATUS', '=', 'AC');
        return $query;
    }

    public function scopeDegree($query)
    {
        $query->where('STRUC_ACAD_PLAN_TYPE', '<>', 'NON');
        return $query;
    }

    public function scopeByCollegeId($query, $college_id = null)
    {
        if ($college_id !== null && $college_id !== '*') {
            $query->where('STRUC_INSTITUTION', '=', $college_id);
        }
        return $query;
    }

    public function scopeByStudentId($query, $student_id)
    {
        $query->where('STRUC_EMPLID', '=', $student_id);
        return $query;
    }

    public function scopeAdmitTermIdLessThan($query, $term_id)
    {
        return $this->whereTermLike('STRUC_PROG_ADMIT_TERM', $query, $term_id);
    }
}
