<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/9/16
 * Time: 12:21 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class ServiceIndicator extends BaseRds implements \Smorken\SisDb\Contracts\Rds\ServiceIndicator
{

    protected $table = 'RDS_SERV_IND_VW';

    protected $columns = [
        'SRVC_EMPLID',
        'SRVC_IND_DATETIME',
        'SRVC_OPRID',
        'SRVC_INSTITUTION',
        'SRVC_SERVICE_IND_CD',
        'SRVC_SERVICE_IND_LDESC',
        'SRVC_SERVICE_IND_REASON',
        'SRVC_SERVICE_IND_REASON_LDESC',
        'SRVC_SERVICE_IND_ACT_TERM',
        'SRVC_SERVICE_IND_ACTIVE_DT',
        'SRVC_POSITIVE_SRVC_INDICATOR',
        'SRVC_AMOUNT',
        'SRVC_COMM_COMMENTS',
        'SRVC_CONTACT',
        'SRVC_DEPARTMENT_LDESC',
    ];

    protected $relation_map = [
        'student' => Student::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'   => 'SRVC_EMPLID',
                'svc_datetime' => function ($attr, $self) {
                    return $this->toCarbonDate('SRVC_IND_DATETIME');
                },
                'entered_by'   => 'SRVC_OPRID',
                'college_id'   => 'SRVC_INSTITUTION',
                'code'         => 'SRVC_SERVICE_IND_CD',
                'description'  => 'SRVC_SERVICE_IND_LDESC',
                'reason_code'  => 'SRVC_SERVICE_IND_REASON',
                'reason_desc'  => 'SRVC_SERVICE_IND_REASON_LDESC',
                'term_id'      => 'SRVC_SERVICE_IND_ACT_TERM',
                'active_date'  => function ($attr, $self) {
                    return $this->toCarbonDate('SRVC_SERVICE_IND_ACTIVE_DT');
                },
                'positive'     => 'SRVC_POSITIVE_SRVC_INDICATOR',
                'amount'       => 'SRVC_AMOUNT',
                'comments'     => 'SRVC_COMM_COMMENTS',
                'contact'      => 'SRVC_CONTACT',
                'department'   => 'SRVC_DEPARTMENT_LDESC',
            ],
        ];
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'SRVC_EMPLID', 'EMPLID')->defaultColumns();
    }

    public function getSRVCINDDATETIMEAttribute($v)
    {
        return $this->toCarbonDate('SRVC_IND_DATETIME');
    }

    public function getSRVCSERVICEINDACTIVEDTAttribute($v)
    {
        return $this->toCarbonDate('SRVC_SERVICE_IND_ACTIVE_DT');
    }

    public function scopeByStudentId($query, $student_id)
    {
        $query->where('SRVC_EMPLID', '=', $student_id);
        return $query;
    }

    public function scopeHolds($query)
    {
        $query->where('SRVC_POSITIVE_SRVC_INDICATOR', '=', 'N');
        $query->whereIn(
            'SRVC_SERVICE_IND_CD',
            [
                'ADX',
                'DBT',
                'ADM',
                'PKH',
                'FAD',
            ]
        );
        return $query;
    }
}
