<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 1:38 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Student extends Person implements \Smorken\SisDb\Contracts\Rds\Student
{

    protected $relation_map = [
        'enrollments'  => Enrollment::class,
        'studentTerms' => StudentTerm::class,
        'groups'       => Group::class,
    ];

    public function memberOf($group)
    {
        foreach ($this->groups as $g) {
            if ($g->id === $group) {
                return true;
            }
        }
        return false;
    }

    public function enrollments()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'EMPLID',
            'EMPLID'
        )
                    ->defaultColumns();
    }

    public function studentTerms()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'CTERM_EMPLID',
            'EMPLID'
        )
                    ->defaultColumns();
    }

    public function groups()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->hasMany(
            $rel,
            'STDGR_EMPLID',
            'EMPLID'
        )
                    ->defaultColumns()
                    ->active();
    }
}
