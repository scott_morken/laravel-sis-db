<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 1:42 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class StudentTerm extends BaseRds implements \Smorken\SisDb\Contracts\Rds\StudentTerm
{

    protected $table = 'RDS_STU_TERM_VW';

    protected $columns = [
        'EMPLID',
        'CTERM_TERM_CD',
        'CTERM_TERM_SDESC',
        'CTERM_INSTITUTION_CODE',
        'CTERM_ACAD_CAREER',
        'STFACT_CUM_GPA',
        'STFACT_TERM_GPA',
        'STFACT_MCCD_CUM_AHRS',
        'STFACT_MCCD_CUM_EHRS',
        'STFACT_MCCD_CUM_QHRS',
        'STFACT_MCCD_CUM_QPTS',
        'STFACT_MCCD_TERM_AHRS',
        'STFACT_MCCD_TERM_EHRS',
        'STFACT_MCCD_TERM_QHRS',
        'STFACT_MCCD_TERM_QPTS',
        'CTERM_ACAD_STDNG_LDESC',
        'CTERM_ACAD_LOAD_SDESC',
        'STFACT_TOT_INPROG_GPA',
        'STFACT_TOT_INPROG_NOGPA',
        'CTERM_ACAD_STNDNG_ACTN',
        'CTERM_ACAD_LOAD_CODE',
    ];

    protected $relation_map = [
        'student' => Student::class,
    ];

    public function conversions()
    {
        return [
            'default' => [
                'student_id'             => 'EMPLID',
                'term_id'                => 'CTERM_TERM_CD',
                'short_term'             => 'CTERM_TERM_SDESC',
                'college_id'             => 'CTERM_INSTITUTION_CODE',
                'acad_type'              => 'CTERM_ACAD_CAREER',
                'cumu_gpa'               => 'STFACT_CUM_GPA',
                'term_gpa'               => 'STFACT_TERM_GPA',
                'cumu_attempted_hours'   => 'STFACT_MCCD_CUM_AHRS',
                'cumu_earned_hours'      => 'STFACT_MCCD_CUM_EHRS',
                'cumu_gpa_hours'         => 'STFACT_MCCD_CUM_QHRS',
                'cumu_gpa_points'        => 'STFACT_MCCD_CUM_QPTS',
                'term_attempted_hours'   => 'STFACT_MCCD_TERM_AHRS',
                'term_earned_hours'      => 'STFACT_MCCD_TERM_EHRS',
                'term_gpa_hours'         => 'STFACT_MCCD_TERM_QHRS',
                'term_gpa_points'        => 'STFACT_MCCD_TERM_QPTS',
                'academic_standing_code' => 'CTERM_ACAD_STDNG_ACTN',
                'academic_standing_desc' => 'CTERM_ACAD_STDNG_LDESC',
                'load_code'              => 'CTERM_ACAD_LOAD_CODE',
                'load_desc'              => 'CTERM_ACAD_LOAD_SDESC',
                'inprogress_gpa'         => 'STFACT_TOT_INPROG_GPA',
                'inprogress_nogpa'       => 'STFACT_TOT_INPROG_NOGPA',
            ],
        ];
    }

    public function name()
    {
        return $this->CTERM_KEY;
    }

    public function student()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'EMPLID', 'EMPLID')->defaultColumns();
    }

    public function getCTERMBEGINDATEAttribute()
    {
        return $this->toCarbonDate('TERM_BEGIN_DT');
    }

    public function getCTERMENDDATEAttribute()
    {
        return $this->toCarbonDate('TERM_END_DT');
    }

    public function scopeOrderReverse($query)
    {
        $query->orderBy('CTERM_INSTITUTION_CODE');
        $query->orderBy('CTERM_TERM_CD', 'desc');
        $query->orderBy('EMPLID');
        return $query;
    }

    public function scopeCredit($query)
    {
        $query->where('CTERM_ACAD_CAREER', '<>', 'NC');
        return $query;
    }

    public function scopeLoad($query)
    {
        $query->where('CTERM_ACAD_LOAD_CODE', '<>', 'N');
        return $query;
    }

    public function scopeLoadAndCredit($query)
    {
        $query->credit()->load();
        return $query;
    }

    public function scopeLessThanEqualTerm($query, $term)
    {
        $query->where(\DB::raw('SUBSTRING(CTERM_TERM_CD, 2, 3)'), '<=', $term);
        return $query;
    }

    public function scopeOrder($query)
    {
        $query->orderBy('CTERM_INSTITUTION_CODE');
        $query->orderBy('CTERM_TERM_CD');
        $query->orderBy('EMPLID');
        return $query;
    }

    public function scopeMaxByHours($query, $hours = 41)
    {
        $query->maxTerm()->greaterThanEqualCredits($hours);
        return $query;
    }

    public function scopeByCollegeId($query, $college_id = null)
    {
        return $this->simpleWhere($query, 'CTERM_INSTITUTION_CODE', $college_id);
    }

    public function scopeByStudentId($query, $student_id)
    {
        return $this->simpleWhere($query, 'EMPLID', $student_id);
    }

    public function scopeByTermId($query, $term_id)
    {
        return $this->whereTermLike('CTERM_TERM_CD', $query, $term_id);
    }

    public function scopeGreaterThanEqualCredits($query, $credits)
    {
        $query->where('STFACT_MCCD_CUM_EHRS', '>=', $credits);
        return $query;
    }

    public function scopeGreaterThanEqualLoad($query, $load)
    {
        $mixes = [
            'N' => ['N', 'L', 'H', 'T', 'F'],
            'L' => ['L', 'H', 'T', 'F'],
            'H' => ['H', 'T', 'F'],
            'T' => ['T', 'F'],
            'F' => ['F'],
        ];
        $load = $load ?: 'L';
        $query->whereIn('CTERM_ACAD_LOAD_CODE', $mixes[$load]);
        return $query;
    }

    public function scopeMaxTerm($query)
    {
        $subquery = self::newQuery()
                        ->selectRaw(
                            'MAX(CTERM_TERM_CD) AS MTERM_CD,
                            EMPLID AS MEMPLID, CTERM_INSTITUTION_CODE as MINSTITUTION_CODE,
                            CTERM_ACAD_CAREER AS MACAD_CAREER'
                        )
                        ->groupBy('EMPLID', 'CTERM_INSTITUTION_CODE', 'CTERM_ACAD_CAREER');
        $query->join(
            \DB::raw(sprintf('(%s) stm', $subquery->toSql())),
            function ($j) {
                $j->on('stm.MTERM_CD', '=', \DB::raw('RDS_STU_TERM_VW.CTERM_TERM_CD'))
                  ->on('stm.MEMPLID', '=', \DB::raw('RDS_STU_TERM_VW.EMPLID'))
                  ->on('stm.MINSTITUTION_CODE', '=', \DB::raw('RDS_STU_TERM_VW.CTERM_INSTITUTION_CODE'))
                  ->on('stm.MACAD_CAREER', '=', \DB::raw('RDS_STU_TERM_VW.CTERM_ACAD_CAREER'));
            }
        );
        return $query;
    }
}
