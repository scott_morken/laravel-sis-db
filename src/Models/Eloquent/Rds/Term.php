<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 10:41 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Rds;

class Term extends BaseRds implements \Smorken\SisDb\Contracts\Rds\Term
{

    protected $table = 'RDS_TERM_VW';

    public function conversions()
    {
        return [
            'default' => [
                'id'          => function ($attr, $self) {
                    return $self->getId($attr);
                },
                'full_id'     => 'STRM',
                'description' => 'DESCR',
                'acad_career' => 'ACAD_CAREER',
                'start_date'  => function ($attr, $self) {
                    return $this->toCarbonDate('TERM_BEGIN_DT');
                },
                'end_date'    => function ($attr, $self) {
                    return $this->toCarbonDate('TERM_END_DT');
                },
                'college_id'  => 'INSTITUTION',
            ],
        ];
    }

    public function name()
    {
        return $this->arrayGet($this->attributes, 'DESCR');
    }

    public function getId($key)
    {
        return substr($this->arrayGet($this->attributes, 'STRM'), 1);
    }

    public function getTERMBEGINDTAttribute()
    {
        return $this->toCarbonDate('TERM_BEGIN_DT');
    }

    public function getTERMENDDTAttribute()
    {
        return $this->toCarbonDate('TERM_END_DT');
    }

    public function scopePast($query)
    {
        $query->whereDate('TERM_END_DT', '<=', date('Y-m-d'));
    }

    public function scopeOrderReverse($query, $limit = 15)
    {
        $query->orderBy('STRM', 'desc')
              ->limit($limit);
        return $query;
    }

    public function scopeActive($query)
    {
        $query->whereDate('TERM_BEGIN_DT', '<=', date('Y-m-d'));
        $query->whereDate('TERM_END_DT', '>=', date('Y-m-d'));
        return $query;
    }

    public function scopeActiveAndUpcoming($query)
    {
        $query->whereDate('TERM_END_DT', '>=', date('Y-m-d'));
        return $query;
    }

    public function scopeCredit($query)
    {
        $query->where('ACAD_CAREER', '<>', 'NC');
        return $query;
    }

    public function scopeCollegeIdIs($q, $college_id)
    {
        return $q->where('INSTITUTION', '=', $college_id);
    }

    public function scopeOrder($query, $limit = 15)
    {
        $query->orderBy('STRM')
              ->limit($limit);
        return $query;
    }

    public function scopeIdLike($query, $term_id)
    {
        return $this->whereTermLike('STRM', $query, $term_id);
    }

    public function scopeLessThanEqualTermId($q, $max_term)
    {
        return $q->where('STRM', '<=', $max_term);
    }

    public function scopeTermIdIs($q, $term_id)
    {
        if (is_array($term_id)) {
            return $q->whereIn('STRM', $term_id);
        }
        return $q->where('STRM', '=', $term_id);
    }

    public function scopeOrTermIdIs($q, $term_id)
    {
        return $q->orWhere('STRM', '=', $term_id);
    }

    public function scopeTermMatchGroup($q, $term_id, $college_id, $acad_career, $type = 'and')
    {
        $method = 'where';
        if ($type === 'or') {
            $method = 'orWhere';
        }
        return $q->$method(
            function ($sq) use ($term_id, $college_id, $acad_career) {
                $sq->where('STRM', '=', $term_id)
                   ->where('INSTITUTION', '=', $college_id)
                   ->where('ACAD_CAREER', '=', $acad_career);
            }
        );
    }
}
