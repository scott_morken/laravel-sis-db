<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/24/17
 * Time: 1:34 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Traits;

trait Converters
{

    /**
     * @param $bool
     * @return int
     */
    protected function boolToInt($bool)
    {
        return $bool ? 1 : 0;
    }
}
