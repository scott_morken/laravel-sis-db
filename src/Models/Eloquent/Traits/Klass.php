<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/24/17
 * Time: 12:59 PM
 */

namespace Smorken\SisDb\Models\Eloquent\Traits;

trait Klass
{

    public function meetings()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function instructors()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function klassNotes()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function courseNotes()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
            ]
        )
                    ->defaultColumns();
    }

    public function fees()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->compositeHasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLSBF_CRSE_ID',
                'CLSBF_CRSE_OFFER_NBR',
                'CLSBF_STRM',
                'CLSBF_SESSION_CD',
                'CLSBF_CLASS_SECTION',
            ]
        )
                    ->defaultColumns();
    }

    public function term()
    {
        $rel = $this->getRelationClass(__FUNCTION__);
        return $this->belongsTo($rel, 'CLASS_TERM_CD', 'STRM');
    }

    public function scopeNotCancelled($query)
    {
        $query->whereNull('CLASS_CANCEL_DATE')
              ->where('CLASS_CLASS_STAT', '<>', 'X');
        return $query;
    }

    public function scopeOpen($query)
    {
        return $this->simpleWhere($query, 'CLASS_ENRL_STAT', 'O');
    }

    public function scopeCourseIs($query, $course_id)
    {
        return $this->simpleWhere($query, 'CLASS_CLASS_NAME', $course_id);
    }

    public function scopeStartAfter($query, $date)
    {
        $query->where('CLASS_START_DATE', '>=', $date);
        return $query;
    }

    public function scopeStartBefore($query, $date)
    {
        $query->where('CLASS_START_DATE', '<=', $date);
        return $query;
    }

    public function scopeEndAfter($query, $date)
    {
        $query->where('CLASS_END_DATE', '>=', $date);
        return $query;
    }

    public function scopeInSession($query)
    {
        $date = date('Y-m-d');
        return $this->scopeInSessionAroundDate($query, $date);
    }

    public function scopeInSessionAroundDate($query, $date)
    {
        $query->where('CLASS_START_DATE', '<=', $date)
              ->where('CLASS_END_DATE', '>=', $date);
        return $query;
    }

    public function scopeOrderTerm($query)
    {
        $query->orderBy('CLASS_INSTITUTION_CD')
              ->orderBy('CLASS_TERM_CD')
              ->orderBy('CLASS_START_DATE');
        return $query;
    }

    public function scopeCollegeIdIs($q, $college_id)
    {
        return $q->where('CLASS_INSTITUTION_CD', '=', $college_id);
    }

    public function scopeClassNumberIs($q, $class_number)
    {
        return $q->where('CLASS_CLASS_NBR', '=', $class_number);
    }

    public function scopeAcademicOrgIdIs($q, $academic_org_id)
    {
        return $q->where('CLASS_ACAD_ORG', '=', $academic_org_id);
    }

    public function scopeTermIdIs($q, $term_id)
    {
        return $q->where('CLASS_TERM_CD', '=', $term_id);
    }

    public function scopeOrderName($q)
    {
        return $q->orderBy('CLASS_CLASS_NAME')
                 ->orderBy('CLASS_CLASS_NBR');
    }

    public function getCLASSCATALOGNBRAttribute($k)
    {
        return trim($this->arrayGet($this->attributes, 'CLASS_CATALOG_NBR'));
    }

    public function getCLASSSTARTDATEAttribute($k)
    {
        return $this->toCarbonDate('CLASS_START_DATE');
    }

    public function getCLASSENDDATEAttribute($k)
    {
        return $this->toCarbonDate('CLASS_END_DATE');
    }

    protected function convertBoolToInt($bool)
    {
        return $bool ? 1 : 0;
    }
}
