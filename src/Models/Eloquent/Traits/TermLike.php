<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 10:57 AM
 */

namespace Smorken\SisDb\Models\Eloquent\Traits;

trait TermLike
{
    protected function whereTermLike($column, $query, $term_id, $alias = null)
    {
        if ($term_id === null) {
            return $query;
        }
        if (strlen($term_id) === 3) {
            $term_id = '_' . $term_id;
            $query->where(
                sprintf('%s.%s', $alias ?: $this->getTable(), $column),
                'LIKE',
                $term_id
            );
        } else {
            $query->where(sprintf('%s.%s', $alias ?: $this->getTable(), $column), '=', $term_id);
        }
        return $query;
    }
}
