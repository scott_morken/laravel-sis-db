<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/15/17
 * Time: 12:25 PM
 */

namespace Smorken\SisDb\Models\VO\MyCC;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class AcademicYear extends Base implements \Smorken\SisDb\Contracts\VO\AcademicYear
{

    protected $end_month = '04';

    protected $end_day = '30';

    public function setEndMonthAndDay($month, $day)
    {
        $this->end_day = $day;
        $this->end_month = $month;
    }

    public function find($id)
    {
        return $this->createModelForYear($id);
    }

    public function endingAfter($date, $limit = 2)
    {
        $point_year = $this->getPointYear($date);
        $collection = new Collection();
        for ($i = 0; $i < $limit; $i++) {
            $collection->push($this->createModelForYear($point_year));
            $point_year += 1;
        }
        return $collection;
    }

    protected function getPointYear($date)
    {
        $point = Carbon::parse($date);
        $point_year = $point->year;
        $start = Carbon::create($point_year, $this->end_month, $this->end_day, 00, 00, 00)->addDay();
        if ($point->lt($start)) {
            return $point_year;
        }
        return $point_year + 1;
    }

    protected function createModelForYear($year)
    {
        $start = Carbon::create($year - 1, $this->end_month, $this->end_day, 00, 00, 00)->addDay();
        $end = $start->copy()->addYear()->subSecond();
        $data = [
            'id'               => $year,
            'description'      => sprintf('%s - %s', $start->year, $end->year),
            'description_long' => sprintf('Fall %s - Spring %s', $start->year, $end->year),
            'start_date'       => $start,
            'end_date'         => $end,
        ];
        return $this->newInstance($data);
    }
}
