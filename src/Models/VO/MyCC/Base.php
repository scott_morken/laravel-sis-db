<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 1:55 PM
 */

namespace Smorken\SisDb\Models\VO\MyCC;

use Carbon\Carbon;
use Smorken\Model\Convertible\Traits\Convert;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Model\VO\Model;

abstract class Base extends Model
{

    use Convert, FakeRelations;

    protected function toCarbonDate($column)
    {
        $value = $this->arrayGet($this->attributes, $column, null);
        if ($value) {
            if ($value instanceof \DateTime) {
                return Carbon::instance($value);
            }
            return Carbon::parse($value);
        }
        return null;
    }

    protected function toFloat($column)
    {
        return (float)$this->arrayGet($this->attributes, $column, 0);
    }

    protected function getFromArray($options, $key)
    {
        return $this->arrayGet($options, $key, $key);
    }

    protected function getRelationClass($relation_name)
    {
        $r = $this->arrayGet($this->relation_map, $relation_name);
        if ($r === null) {
            throw new \InvalidArgumentException("$relation_name is not a valid relation.");
        }
        return $r;
    }

    protected function arrayGet($arr, $key, $default = null)
    {
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        $key = strtolower($key);
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        return $default;
    }
}
