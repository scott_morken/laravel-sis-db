<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 9:32 AM
 */
namespace Tests\Smorken\SisDb\unit\Model\Cds;

use Mockery as m;

class EnrollmentTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
    
    public function testClassJoin()
    {
        $res = m::mock('Illuminate\Database\ConnectionResolverInterface');
        $conn = m::mock('Illuminate\Database\ConnectionInterface');
        $grammar = m::mock('Illuminate\Database\Query\Grammars\Grammar');
        $proc = m::mock('Illuminate\Database\Query\Processors\Processor');
        $res->shouldReceive('connection')->andReturn($conn);
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn($proc);
        $grammar->shouldReceive('getDateFormat')->andReturn('Y-m-d');
        $grammar->shouldReceive('compileInsertGetId')->andReturn('id');
        $grammar->shouldReceive('compileSelect')->andReturn('foo select');
        $proc->shouldReceive('processInsertGetId')->andReturn('id');
        \Smorken\SisDb\Models\Eloquent\Cds\Enrollment::setConnectionResolver($res);
        $e = new \Smorken\SisDb\Models\Eloquent\Cds\Enrollment();
        $e->forceFill(['ENRL_TERM_CD' => 1111, 'CLASS_CLASS_NBR' => 12345]);
        $this->assertEquals('foo select', $e->klass()->toSql());
    }
}
