<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/15/17
 * Time: 12:41 PM
 */

namespace Tests\Smorken\SisDb\unit\Model\VO\MyCC;

use Illuminate\Support\Collection;
use Smorken\SisDb\Models\VO\MyCC\AcademicYear;

class AcademicYearTest extends \PHPUnit\Framework\TestCase
{

    public function testGetById()
    {
        $sut = new AcademicYear();
        $m = $sut->find('2017');
        $this->assertEquals('2017', $m->id);
        $this->assertEquals('2016 - 2017', $m->description);
        $this->assertEquals('Fall 2016 - Spring 2017', $m->description_long);
        $this->assertEquals('2016-05-01 00:00:00', $m->start_date->toDateTimeString());
        $this->assertEquals('2017-04-30 23:59:59', $m->end_date->toDateTimeString());
    }

    public function testEndingAfterIsAfterStart()
    {
        $sut = new AcademicYear();
        $coll = $sut->endingAfter('2017-08-01');
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertEquals('2018', $coll->first()->id);
        $this->assertEquals('2019', $coll->last()->id);
    }

    public function testEndingAfterIsBeforeStart()
    {
        $sut = new AcademicYear();
        $coll = $sut->endingAfter('2017-01-01');
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertEquals('2017', $coll->first()->id);
        $this->assertEquals('2018', $coll->last()->id);
    }
}
